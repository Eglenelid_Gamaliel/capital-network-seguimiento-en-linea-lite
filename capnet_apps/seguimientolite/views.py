import os
import json
import hashlib
from datetime import datetime
from django.conf import settings
from django.shortcuts import render, redirect
from django.utils.safestring import mark_safe
from django.views import generic
from django.views.decorators.http import require_POST
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.core.files.storage import FileSystemStorage
from jfu.http import upload_receive, UploadResponse, JFUResponse
from django.http import FileResponse
from django.views.decorators.http import require_POST
from django.contrib.auth.models import User, Group, Permission
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login
from webpush.models import PushInformation
from django.core.mail import EmailMessage
import fitz
from .models import *
from .pdf_camiones import get_pdf
from .pdf_calidad import get_pdf_calidad
import base64
import mimetypes
from PIL import Image
import numpy as np
import io

def staff_login(request):
    form = AuthenticationForm()
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        #if form.is_valid():
        username = request.POST['username']
        password = request.POST['password']
        password_view = hashlib.sha1(bytes(password, encoding='utf-8'))
        print(password_view.hexdigest())
        user_in_local = authenticate(username=username, password=password)
        try:
            user_in_view = VUsuarios.objects.get(cveusuario=username, pass_field=str(password_view.hexdigest()).upper())
        except Exception as e:
            print(e)
            print('El usuario no esta en la vista')
            user_in_view = False
        if user_in_view and user_in_local:
            if user_in_view.cveperfil == 2:
                login(request, user_in_local)
                return redirect('/asesor/')
            elif user_in_view.cveperfil == 3:
                login(request, user_in_local)
                return redirect('/jefedetaller/')
            elif user_in_view.cveperfil == 7:
                login(request, user_in_local)
                return redirect('/refacciones/')
            elif user_in_view.cveperfil == 1:
                login(request, user_in_local)
                return redirect('/asesor/')
        elif user_in_view and not user_in_local:
            new_local_user = User.objects.create_user(username=username, first_name=user_in_view.nombre)
            new_local_user.set_password(password)
            new_local_user.save()
            print('creando usuario')
            login(request, new_local_user)
            return redirect('/asesor/')
        else:
            print('No en vista')
    return render(request, 'seguimientolite/login.html', {'form': form})


def tecnico(request, id_tecnico, no_orden):
    context = {}
    if request.method == 'GET':
        try:
            a = LogGeneral.objects.get(no_orden=no_orden)
            if not a.fin_tecnico:
                a.inicio_tecnico=datetime.now()
                a.save()
        except Exception as e:
            print(e)
            LogGeneral.objects.create(no_orden=no_orden, inicio_tecnico=datetime.now())
        context = {}
        try:
            info = VOperacionesTecnico.objects.filter(no_orden=no_orden).order_by('-fecha_llegada').first()
            context['filas'] = ActividadesTecnico.objects.filter(no_orden=no_orden).order_by('-fecha_hora_fin')
            context['info'] = info
            context['lista_items'] = ListaItemsCalidad.objects.exclude(familia='Prueba de Ruta').values_list('descripcion', flat=True)
            context['calidad'] = ActividadesTecnicoCalidad.objects.get(no_orden=no_orden)
        except Exception as e:
            print(e)
    if request.is_ajax():
        r = request.POST
        print(r)
        
        try:
            info = VOperacionesTecnico.objects.filter(no_orden=no_orden).order_by('-fecha_llegada').first()
            context['filas'] = ActividadesTecnico.objects.filter(no_orden=no_orden)
            context['info'] = info
            context['lista_items'] = ListaItemsCalidad.objects.exclude(familia='Prueba de Ruta').values_list('descripcion', flat=True)
        except Exception as e:
            print(e)
        try:
            update_one = r.get('update_one', None)
            remove_one = r.get('remove_one', None)
            if update_one:
                update = ActividadesTecnico.objects.get(no_orden=no_orden, item=r['item'])
                update.estado = r['estado']
                update.fecha_hora_actualizacion = datetime.now()
                update.save()
            elif remove_one:
                ActividadesTecnico.objects.get(no_orden=no_orden, item=r['item']).delete()
            elif r['estado'] is not False:
                ActividadesTecnico.objects.create(
                    no_orden=no_orden,
                    #num_tor=r['num_tor'],
                    id_hd=info.id_hd,
                    tecnico=id_tecnico,
                    item=r['item'],
                    estado=request.POST.get('estado', False),
                    comentarios=r['comentario'],
                    #fecha_hora_inicio=inicio,
                    fecha_hora_fin=datetime.now()
                )
                print('item registrado')
                for i in range(int(r['numEvidencia'])):
                    ActividadesTecnicoMedios.objects.create(
                        no_orden=no_orden,
                        item=r['item'],
                        evidencia=r[f'media[{i}]'],
                        fecha_hora_subida=datetime.now()
                    )
                b = LogGeneral.objects.get(no_orden=no_orden)
                b.fin_tecnico = datetime.now()
                b.save()
                
        except Exception as e:
            print(e)
            print('query no realizado')
        
        try:
            calidad = ActividadesTecnicoCalidad.objects.get(no_orden=no_orden)
            
            calidad.presion_del_izquierda=r['presion_del_izquierda']
            calidad.presion_del_derecha=r['presion_del_derecha']
            calidad.presion_tras_izquierda=r['presion_tras_izquierda']
            calidad.presion_tras_derecha=r['presion_tras_derecha']
            calidad.presion_refaccion=r['presion_refaccion']
            calidad.estado_del_izquierda=r['estado_del_izquierda']
            calidad.estado_del_derecha=r['estado_del_derecha']
            calidad.estado_tras_izquierda=r['estado_tras_izquierda']
            calidad.estado_tras_derecha=r['estado_tras_derecha']
            
            print(calidad)
            calidad.save()
        except Exception as e:
            print(e)
            ActividadesTecnicoCalidad.objects.create(
                no_orden=no_orden,
                presion_del_izquierda=r['presion_del_izquierda'],
                presion_del_derecha=r['presion_del_derecha'],
                presion_tras_izquierda=r['presion_tras_izquierda'],
                presion_tras_derecha=r['presion_tras_derecha'],
                presion_refaccion=r['presion_refaccion'],
                estado_del_izquierda=r['estado_del_izquierda'],
                estado_del_derecha=r['estado_del_derecha'],
                estado_tras_izquierda=r['estado_tras_izquierda'],
                estado_tras_derecha=r['estado_tras_derecha']
            )


    return render(request, 'seguimientolite/tecnico.html', context)


def recepcionactiva(request, placas):
    inicio = datetime.now()
    context = {}
    context['placas'] = placas.upper()
    try:
        info = VOperacionesRecepcionActiva.objects.filter(noplacas=placas).order_by('-horallegada')[0]
        context['info'] = info
        context['lista_tecnicos'] = Tecnicos.objects.exclude(nombre_empleado='NO SHOW').values_list('nombre_empleado', flat=True)
        try:
            LogRecepcionActiva.objects.get(no_orden=info.noorden).update(inicio_recepcion=datetime.now())
        except Exception as e:
            print(e)
            print('Creating New LOG')
            LogRecepcionActiva.objects.create(no_orden=info.noorden, inicio_recepcion=datetime.now())
        if request.is_ajax():
            r = request.POST
            try:
                ActividadesTecnicoRecepcionActiva.objects.update_or_create(
                    id_hd=info.id_hd,
                    placas=placas,
                    item=r['item'],
                    tecnico=r['tecnico'],
                    estado=r['estado'],
                    comentarios=r['comentario'],
                    fecha_hora_inicio=inicio,
                    fecha_hora_fin=datetime.now())
                LogGeneral.objects.get(no_orden=no_orden).update(fin_recepcion=datetime.now())
                for i in range(int(r['numEvidencia'])):
                    try:
                        ActividadesTecnicoMediosRecepcionActiva.objects.update_or_create(
                            id_hd=context['info']['id_hd'],
                            placas=placas,
                            item=r['item'],
                            evidencia=r[f'media[{i}]'],
                            fecha_hora_subida=datetime.now())
                    except Exception as e:
                        print(e)
            except Exception as e:
                print(e)
    except Exception as e:
        print(e)
        print('Couldnt get info or Tech list')
    return render(request, 'seguimientolite/recepcionactiva.html', context)


def refecciones(request):
    if not request.user.is_authenticated:
        return redirect('/login/')
    try:
        context = {}
        context['filas'] = VOperacionesRefacciones.objects.distinct().order_by('-no_orden')
    except Exception as e:
        print(e)
    return render(request, 'seguimientolite/refacciones.html', context)


def refacciones_detalle(request, no_orden):
    if not request.user.is_authenticated:
        return redirect('/login/')
    try:
        log = LogGeneral.objects.get(no_orden=no_orden)
        if not log.inicio_refacciones and not log.fin_refacciones:
            log.inicio_refacciones = datetime.now()
            log.save()
        if log.inicio_refacciones and log.fin_refacciones:
            pass
    except Exception as e:
        print(e)
    if settings.SEGUIMIENTOLITE['INTERFAZ_REFACCIONES']:
        try:
            context = {}
            context['orden'] = VOperacionesRefacciones.objects.get(no_orden=no_orden)
            filas = ActividadesTecnico.objects.filter(no_orden=no_orden)
            context['filas'] = filas
            numeros_tor = filas.values_list('num_tor', flat=True).distinct()
            print(numeros_tor)
            context['refacciones'] = VInterfazCotizacionesRef.objects.filter(no_orden__contains=context['filas'][0].num_tor)
            context['filas_media'] = [] #ActividadesTecnicoMedios.objects.filter(no_orden=no_orden)
            context['filas_video'] = []
            media = ActividadesTecnicoMedios.objects.filter(no_orden=no_orden)
            for file in media:
                file_type = mimetypes.guess_type(file.evidencia)[0]
                try:
                    if 'video' in file_type:
                        context['filas_video'].append(file)
                    else:
                        context['filas_media'].append(file)
                except Exception as e:
                    print(e)
            context['pdf'] = ActividadesRefaccionesCotizacionPdf.objects.get(
                no_orden=no_orden)
            print(context['pdf'])
            
        except Exception as e:
            print(e)
        return render(request, 'seguimientolite/refacciones_detalle.html', context)
    else:
        context = {}
        context['orden'] = VOperacionesRefacciones.objects.filter(no_orden=no_orden).first()
        filas = ActividadesTecnico.objects.filter(no_orden=no_orden)
        try:
            context['cotizados'] = ActividadesRefaccionesAlt.objects.filter(no_orden=no_orden).order_by('item')
            context['filas'] = filas.exclude(item__in=context['cotizados'].values_list('item', flat=True))
        except Exception as e:
            print(e)
            context['filas'] = filas
        #context['filas_media'] = ActividadesTecnicoMedios.objects.filter(no_orden=no_orden)
        context['filas_media'] = []
        context['filas_video'] = []
        media = ActividadesTecnicoMedios.objects.filter(no_orden=no_orden)
        for file in media:
            file_type = mimetypes.guess_type(file.evidencia)[0]
            try:
                if 'video' in file_type:
                    context['filas_video'].append(file)
                else:
                    context['filas_media'].append(file)
            except Exception as e:
                print(e)
            
        context['iva'] = settings.SEGUIMIENTOLITE['IVA']
        if request.is_ajax():
            r = request.POST
            try:
                a = LogGeneral.objects.get(no_orden=no_orden)
                a.fin_refacciones = datetime.now()
                a.save()
            except Exception as e:
                print(e)
            try:
                update = ActividadesRefaccionesAlt.objects.get(no_orden=no_orden, nombre=r['nombre'])
                update.no_orden=no_orden
                update.item=r['item']
                update.id_hd=filas[0].id_hd
                update.cantidad=r['cantidad']
                update.no_parte=r['noparte']
                update.nombre=r['nombre']
                update.precio=r['precio']
                update.precio_iva=r.get('precioiva', 0)
                update.existencia=r['existencia']
                update.localizacion=r['localizacion']
                update.fecha_hora_fin=datetime.now()
                update.save()
                print('Encontrado')
            except Exception as e:
                ActividadesRefaccionesAlt.objects.create(
                    no_orden=no_orden,
                    item=r['item'],
                    id_hd=filas[0].id_hd,
                    cantidad=r['cantidad'],
                    no_parte=r['noparte'],
                    nombre=r['nombre'],
                    precio=r['precio'],
                    precio_iva=r.get('precioiva', 0),
                    existencia=r['existencia'],
                    localizacion=r['localizacion'],
                    fecha_hora_fin=datetime.now()
                )
                print(e)
                print('creado')
        return render(request, 'seguimientolite/refacciones_detalle_alt.html', context)


def recepcionactiva_detalle(request, id_hd):
    try:
        context = {}
        context['filas'] = ActividadesTecnicoRecepcionActiva.objects.filter(id_hd=id_hd)
        context['orden'] = VOperacionesAsesorRecepcionActiva.objects.get(
            id_hd=id_hd)
        try:
            log = LogRecepcionActiva.objects.get(placas=context['orden'].no_orden)
            if log.inicio_cotizacion and not log.fin_cotizacion:
                log.update(fin_cotizacion=datetime.now())
            if log.inicio_cotizacion and log.fin_cotizacion:
                pass
        except Exception as e:
            print(e)
        context['filas_media'] = ActividadesTecnicoMediosRecepcionActiva.objects.filter(
            id_hd=id_hd)
        context['pdf'] = ActividadesRefaccionesCotizacionPdf.objects.get(
            id_hd=id_hd)
        print(context['pdf'])
    except Exception as e:
        print(e)
    if request.method == 'POST':
        context = {}
        print(request.POST)
        info = VOperacionesAsesorRecepcionActiva.objects.get(id_hd=id_hd)
        post = request.POST
        try:
            LogGeneral.objects.get(no_orden=no_orden).update(fin_refacciones=datetime.now())
        except Exception as e:
            print(e)
        ActividadesAsesorRecepcionActiva.objects.update_or_create(
            id_hd=id_hd,
            no_orden=info.no_orden,
            placas=info.placas,
            vin=info.vin,
            cliente=info.cliente,
            vehiculo=info.vehiculo,
            asesor=info.asesor,
            tecnico=info.tecnico,
            item=post['item'],
            estado=post['estado'],
            comentarios=post['comentarios'],
            mo=post['mano_de_obra'],
            refacciones=post['refacciones'],
            #total=,
            fecha_hora_guardado=datetime.now(),
            #fecha_hora_modificacion=,
        )
    return render(request, 'seguimientolite/recepcionactiva_detalle.html', context)


def jefe_de_taller(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    context = {}
    try:
        context = {}
        context['filas'] = VOperacionesRefacciones.objects.distinct()
    except Exception as e:
        print(e)
    return render(request, 'seguimientolite/jefe_de_taller.html', context)


def jefe_de_taller_detalle(request, no_orden):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    try:
        log = LogGeneral.objects.get(no_orden=no_orden)
        if log.inicio_jdt and not log.fin_jdt:
            log.update(fin_jdt=datetime.now())
        if log.inicio_jdt and log.fin_jdt:
            pass
    except Exception as e:
        print(e)
    context = {}
    if request.method == 'GET':
        try:
            context['orden'] = VOperacionesRefacciones.objects.get(no_orden=no_orden)
            context['filas'] = ActividadesTecnico.objects.filter(no_orden=no_orden)
            context['refacciones'] = VInterfazCotizacionesMo.objects.filter(vin__contains=context['orden'].vin)
            context['filas_media'] = [] #ActividadesTecnicoMedios.objects.filter(no_orden=no_orden)
            context['filas_video'] = []
            media = ActividadesTecnicoMedios.objects.filter(no_orden=no_orden)
            for file in media:
                file_type = mimetypes.guess_type(file.evidencia)[0]
                try:
                    if 'video' in file_type:
                        context['filas_video'].append(file)
                    else:
                        context['filas_media'].append(file)
                except Exception as e:
                    print(e)
            context['pdf'] = ActividadesRefaccionesCotizacionPdf.objects.filter(
                no_orden=no_orden)[0]
            print(context['pdf'])
        except Exception as e:
            print(e)
    elif request.method == 'POST':
        LogGeneral.objects.get(no_orden=no_orden).update(fin_jdt=datetime.now())
        fs = FileSystemStorage()
        uploaded_file = request.FILES.get('pdf', None)
        fs.save(uploaded_file.name, uploaded_file)
        ActividadesRefaccionesCotizacionPdf.objects.update_or_create(
            no_orden=no_orden,
            fecha_hora_fin=datetime.now(),
            nombre_pdf=uploaded_file.name,
        )
        pdffile = os.path.join(settings.MEDIA_ROOT, uploaded_file.name)
        doc = fitz.open(pdffile)
        print(doc.pageCount)

        for i in range(doc.pageCount):
            page = doc.loadPage(i) #number of page
            pix = page.getPixmap()
            output = os.path.join(settings.MEDIA_ROOT, f'{no_orden}{i}.png')
            pix.writePNG(output)
    return render(request, 'seguimientolite/jefe_de_taller_detalle.html', context)


def recepcionactiva_cotizacion(request, id_hd):
    context = {}
    try:
        PushInformation.objects.get(user=request.user)
        context['user_have_push'] = True
    except Exception as e:
        print(e)
        context['user_have_push'] = False
    try:
        context = {}
        a = ActividadesTecnico.objects.filter(no_orden=no_orden)
        context['filas'] = a
        context['buen_estado'] = a.filter(estado='Buen Estado').count()
        context['recomendado'] = a.filter(estado='Recomendado').count()
        context['urgente'] = a.filter(estado='Inmediato').count()
        context['orden'] = VOperacionesRefacciones.objects.get(
            no_orden=no_orden)
        ev = VSeguimientoEvidencia.objects.filter(no_orden=no_orden)
        context['buen_estado_ev'] = ev.filter(estado='Buen Estado')
        context['recomendado_ev'] = ev.filter(estado='Recomendado')
        context['urgente_ev'] = ev.filter(estado='Inmediato')
        context['pdf'] = ActividadesRefaccionesCotizacionPdf.objects.get(
            no_orden=no_orden)
    except Exception as e:
        print(e)
    return render(request, 'seguimientolite/recepcionactiva_cotizacion.html', context)


def calidad(request):
    context = {}
    return render(request, 'seguimientolite/calidad.html')


def calidad_detalle(request, no_orden):
    context = {}
    try:
        revisados = ActividadesCalidad.objects.filter(no_orden=no_orden)
        if revisados:
            context['items_revisados'] = revisados
        else:
            queryset_items = ListaItemsCalidad.objects.filter(familia='Prueba de Ruta')
            context['items_revision'] = queryset_items.values_list('descripcion', flat=True)
    except Exception as e:
        print(e)
        queryset_items = ListaItemsCalidad.objects.filter(familia='Prueba de Ruta')
        context['items_revision'] = queryset_items.values_list('descripcion', flat=True)
    context['orden'] = VOperacionesRefacciones.objects.filter(no_orden=no_orden).first()
    context['filas_media'] = []
    context['filas_video'] = []
    media = ActividadesTecnicoMedios.objects.filter(no_orden=no_orden)
    for file in media:
        file_type = mimetypes.guess_type(file.evidencia)[0]
        try:
            if 'video' in file_type:
                context['filas_video'].append(file)
            else:
                context['filas_media'].append(file)
        except Exception as e:
            print(e)
    if request.method == 'POST':
        #print(request.POST)
        for item in request.POST:
            if item != 'csrfmiddlewaretoken':
                print(item)
                print(request.POST[item])
                try:
                    update_item = ActividadesCalidad.objects.get(no_orden=no_orden, item=item)
                    update_item.estado = request.POST[item]
                    update_item.save()
                except Exception as e:
                    print(e)
                    ActividadesCalidad.objects.create(no_orden=no_orden, item=item, estado=request.POST[item])
    return render(request, 'seguimientolite/calidad_detalle.html', context)


def asesor(request):
    if not request.user.is_authenticated:
        return redirect('/login/')
    context = {}
    try:
        PushInformation.objects.get(user=request.user)
        context['user_have_push'] = True
    except Exception as e:
        print(e)
        context['user_have_push'] = False
    try:
        if request.user.username == 'ADMIN' or 'admin':
            filas_ordenes = VOperacionesAsesorAlt.objects.order_by('-no_orden').all()
            ordenes = filas_ordenes.values_list('no_orden', flat=True)
        else:
            asesor_name = VUsuarios.objects.get(cveusuario=request.user.username)
            #context['filas'] = VOperacionesAsesor.objects.filter(asesor=asesor_name.nombre).distinct()    
            filas_ordenes = VOperacionesAsesorAlt.objects.order_by('no_orden').filter(asesor=asesor_name.nombre).distinct()
            ordenes = filas_ordenes.values_list('no_orden', flat=True)
        logs = {}
        envios = {}
        context['filas'] = []
        try:
            for orden in ordenes:
                log = LogGeneral.objects.get(no_orden=orden)
                if log.inicio_tecnico and not log.fin_tecnico:
                    state = 'En Revisión'
                elif log.inicio_tecnico and log.fin_tecnico and not log.inicio_refacciones:
                    state = 'Esperando Refacciones'
                elif log.inicio_refacciones and not log.fin_refacciones:
                    state = 'Llenando Refacciones'
                elif log.inicio_refacciones and log.fin_refacciones and not log.inicio_jdt:
                    state = 'Esperando Mano de Obra'
                elif log.inicio_jdt and not log.fin_jdt:
                    state = 'Llenando Mano de Obra'
                elif log.inicio_jdt and log.fin_jdt and not log.inicio_asesor:
                    state = 'Esperando Revision Asesor'
                elif log.inicio_asesor and not log.fin_asesor:
                    state = 'En Revision Asesor'
                elif log.inicio_asesor and log.fin_asesor:
                    state = 'Enviado'
                order_info = filas_ordenes.filter(no_orden=orden)[0]
                try:
                    visits = LogCliente.objects.filter(no_orden=orden).count()
                except Exception as e:
                    print(e)
                    visits = 0
                try:
                    respuesta = ActividadesCliente.objects.filter(no_orden=orden, autorizacion='Autorizado').count()
                except Exception as e:
                    print(e)
                    respuesta = 0
                
                fecha_ingreso = log.inicio_tecnico
                fecha_hora_fin_refacciones = log.fin_refacciones

                context['filas'].append(
                    {
                        "fecha_ingreso": fecha_ingreso,
                        "no_orden": order_info.no_orden,
                        "placas": order_info.placas,
                        "vin": order_info.vin,
                        "vehiculo": order_info.vehiculo,
                        "asesor": order_info.asesor,
                        "fecha_hora_fin_refacciones": fecha_hora_fin_refacciones,
                        "estado": state,
                        "visitas":  visits,
                        "respuesta": respuesta
                    }
                )
        except Exception as e:
            print(e)
    except Exception as e:
        print(e)
    return render(request, 'seguimientolite/ordenes.html', context)


def asesor_detalle(request, no_orden):
    if not request.user.is_authenticated:
        return redirect('/login/')
    try:
        log = LogGeneral.objects.get(no_orden=no_orden)
        if log.fin_refacciones and not log.fin_asesor:
            log.inicio_asesor = datetime.now()
            log.save()
        #if log.inicio_asesor and not log.fin_asesor:
        #    log.fin_asesor = datetime.now())
        if log.inicio_asesor and log.fin_asesor:
            pass
    except Exception as e:
        print(e)
    context = {}
    if settings.SEGUIMIENTOLITE['INTERFAZ_REFACCIONES']:
        try:
            a = ActividadesTecnico.objects.filter(no_orden=no_orden)
            context['filas'] = a
            context['orden'] = VOperacionesRefacciones.objects.get(
                no_orden=no_orden)
            #context['filas_media'] = ActividadesTecnicoMedios.objects.filter(
            #    no_orden=no_orden)
            context['filas_media'] = []
            context['filas_video'] = []
            media = ActividadesTecnicoMedios.objects.filter(no_orden=no_orden)
            for file in media:
                file_type = mimetypes.guess_type(file.evidencia)[0]
                try:
                    if 'video' in file_type:
                        context['filas_video'].append(file)
                    else:
                        context['filas_media'].append(file)
                except Exception as e:
                    print(e)
            context['pdf'] = ActividadesRefaccionesCotizacionPdf.objects.filter(
                no_orden=no_orden)[0]
            print(context['pdf'])
        except Exception as e:
            print(e)
        if request.is_ajax():
            LogGeneral.objects.get(no_orden=no_orden).update(fin_asesor=datetime.now())
            if request.POST['metodo'] == 'E-Mail':
                try:
                    pdf = ActividadesRefaccionesCotizacionPdf.objects.filter(no_orden=no_orden).values_list('nombre_pdf', flat=True)[0]
                except Exception as e:
                    print(e)
                    pdf = None
                client_mail = request.POST.get('mail', None)
                try:
                    email = EmailMessage(
                        'Seguimiento en linea',
                        f'http://200.32.68.6:3001/cliente/{no_orden}/',
                        settings.EMAIL_HOST_USER,
                        [client_mail]
                        #['bcc@example.com'],
                        #reply_to=['another@example.com'],
                        #headers={'Message-ID': 'foo'},
                    )
                    if pdf:
                        email.attach_file(os.path.join(settings.MEDIA_ROOT, pdf))
                    else:
                        pass
                    email.send()
                    print('mensaje enviado')
                except Exception as e:
                    print(e)
                LogEnvios.objects.create(
                    no_orden=no_orden,
                    medio=request.POST['metodo'],
                    fecha_hora_envio=datetime.now(),
                    correo=request.POST['mail']
                )
            elif request.POST['metodo'] == 'WhatsApp':
                LogEnvios.objects.create(
                    no_orden=no_orden,
                    medio=request.POST['metodo'],
                    fecha_hora_envio=datetime.now,
                    telefono=request.POST['telefono']
                )
        return render(request, 'seguimientolite/detalle_orden.html', context)    
    else:
        try:
            context['precio_ut'] = settings.SEGUIMIENTOLITE['PRECIO_UT']
            context['iva'] = settings.SEGUIMIENTOLITE['IVA']
            #context['orden'] = VOperacionesRefacciones.objects.get(no_orden=no_orden)
            context['orden'] = VOperacionesRefacciones.objects.filter(no_orden=no_orden).first()
            #context['filas_media'] = ActividadesTecnicoMedios.objects.filter(no_orden=no_orden)
            context['filas_media'] = []
            context['filas_video'] = []
            media = ActividadesTecnicoMedios.objects.filter(no_orden=no_orden)
            for file in media:
                file_type = mimetypes.guess_type(file.evidencia)[0]
                try:
                    if 'video' in file_type:
                        context['filas_video'].append(file)
                    else:
                        context['filas_media'].append(file)
                except Exception as e:
                    print(e)
            
            try:
                context['filas_completado'] = ActividadesAsesorAlt.objects.filter(no_orden=no_orden).order_by('item')
                context['filas'] = ActividadesRefaccionesAlt.objects.filter(no_orden=no_orden).exclude(nombre__in=context['filas_completado'].values_list('refaccion', flat=True))
                print('encuentra en asesor')
                try:
                    context['cliente'] = ActividadesCliente.objects.filter(no_orden=no_orden).order_by('item')
                except Exception as e:
                    print(e)
            except Exception as e:
                print(e)
                print('no encuentra en asesor')
                context['filas'] = ActividadesRefaccionesAlt.objects.filter(no_orden=no_orden)
            

            #context['pdf'] = ActividadesRefaccionesCotizacionPdf.objects.filter(no_orden=no_orden)[0]
            #print(context['pdf'])
        except Exception as e:
            print(e)
        if request.is_ajax():
            r = request.POST
            metodo = r.get('metodo', None)
            if metodo == 'E-Mail':
                try:
                    pdf = ActividadesRefaccionesCotizacionPdf.objects.filter(no_orden=no_orden).values_list('nombre_pdf', flat=True)[0]
                except Exception as e:
                    print(e)
                    pdf = None
                client_mail = request.POST.get('mail', None)
                try:
                    body = f'''Estimado cliente, necesitamos de su atención para revisar la cotización.\n\nIngresa al enlace desde:\n\ncpnetwork.grupomavesa.com.ec:3001/cliente/{no_orden}/'''
                    email = EmailMessage(
                        'Seguimiento en linea Citröen Mavesa',
                        body,
                        'talleres@mavesaec.com.ec', #settings.EMAIL_HOST_USER,
                        [client_mail]
                        #['bcc@example.com'],
                        #reply_to=['another@example.com'],
                        #headers={'Message-ID': 'foo'},
                    )
                    if pdf:
                        email.attach_file(os.path.join(settings.MEDIA_ROOT, pdf))
                    else:
                        pass
                    email.send()
                    print('mensaje enviado')
                except Exception as e:
                    print(e)
                LogEnvios.objects.create(
                    no_orden=no_orden,
                    medio=request.POST['metodo'],
                    fecha_hora_envio=datetime.now(),
                    correo=request.POST['mail']
                )
            else:
                sign = r.get('firma', None)
                if sign:
                    try:
                        print(sign[0][22:])
                        data = sign[22:].encode()
                        with open(r"C:/inetpub/wwwroot/Capnet_Apps_Citroen_rev/media/tmp.png", "wb") as fh:
                            fh.write(base64.decodebytes(data))
                        #img = base64.decodebytes(data)
                        threshold=100
                        dist=5
                        #img_buffer = Image.open(img).convert('RGBA')
                        img_buffer = Image.open(r"C:/inetpub/wwwroot/Capnet_Apps_Citroen_rev/media/tmp.png").convert('RGBA')
                        print('aaa')
                        #img_buffer = Image.frombuffer(img)
                        # np.asarray(img) is read only. Wrap it in np.array to make it modifiable.
                        arr = np.array(np.asarray(img_buffer))
                        r,g,b,a=np.rollaxis(arr,axis=-1)    
                        mask=((r>threshold)
                            & (g>threshold)
                            & (b>threshold)
                            & (np.abs(r-g)<dist)
                            & (np.abs(r-b)<dist)
                            & (np.abs(g-b)<dist)
                            )
                        arr[mask,3]=0
                        name = f'{datetime.now()}.png'
                        img_buffer = Image.fromarray(arr,mode='RGBA')
                        img_buffer.save(os.path.join(settings.MEDIA_ROOT, f'{no_orden}.png'))
                        os.remove(r"C:/inetpub/wwwroot/Capnet_Apps_Citroen_rev/media/tmp.png")
                        try:
                            f = ActividadesAsesorFirmas.objects.get(no_orden=no_orden)
                            f.firma = f'{no_orden}.png'
                            f.save()
                        except Exception as e:
                            print(e)
                            ActividadesAsesorFirmas.objects.create(no_orden=no_orden, firma=f'{no_orden}.png')
                    except Exception as e:
                        print(e)
                else:
                    log = LogGeneral.objects.get(no_orden=no_orden)
                    log.fin_asesor = datetime.now()
                    log.save()
                    try:
                        update = ActividadesAsesorAlt.objects.get(no_orden=no_orden, refaccion=r['refaccion'])
                        update.id_hd=context['filas'][0].id_hd
                        update.no_orden=no_orden
                        update.placas=context['orden'].placas
                        update.vin=context['orden'].vin
                        update.item=r['item']
                        update.refaccion=r['refaccion']
                        update.precio_iva=r['precio_iva']
                        update.existencia=r['existencia']
                        update.localizacion=r['localizacion']
                        update.garantia_seguro=r['garantia']
                        update.rvt=r['rvt']
                        update.cantidad_ut=r['uts']
                        update.mo_iva=r.get('mo_iva', 0)
                        update.total=r['total']
                        update.save()
                    except Exception as e:
                        print(e)
                        ActividadesAsesorAlt.objects.create(
                            id_hd=context['filas'][0].id_hd,
                            no_orden=no_orden,
                            placas=context['orden'].placas,
                            vin=context['orden'].vin,
                            item=r['item'],
                            refaccion=r['refaccion'],
                            precio_iva=r['precio_iva'],
                            existencia=r['existencia'],
                            localizacion=r['localizacion'],
                            garantia_seguro=r['garantia'],
                            rvt=r['rvt'],
                            cantidad_ut=r['uts'],
                            descuento_mo=r['desc_mo'],
                            descuento_ref=r['desc_ref'],
                            #mo=r['mano_de_obra'],
                            mo_iva=r.get('mo_iva', 0),
                            total=r['total'],
                            #fecha_hora_fin=datetime.now()
                        )
        return render(request, 'seguimientolite/asesor_alt.html', context)


def client_autologin(request, no_orden):
    try:
        client = User.objects.get(username=no_orden)
        login(request, client)
        return HttpResponseRedirect(f'/cotizacion/{no_orden}')
    except Exception:
        client = User.objects.create_user(username=no_orden)
        group = Group.objects.get_or_create(name='cliente')[0]
        password = User.objects.make_random_password(length=12)
        client.set_password(password)
        client.groups.add(group)
        client.save()
        login(request, client)
        return HttpResponseRedirect(f'/cotizacion/{no_orden}')


def cotizacion_cliente(request, no_orden):
    context = {}
    try:
        m = ActividadesCliente.objects.filter(no_orden=no_orden)
        c = ActividadesAsesorAlt.objects.filter(no_orden=no_orden).order_by('item')
        a = c.exclude(refaccion__in=m.values_list('refaccion', flat=True))
    except expression as identifier:
        a = ActividadesAsesorAlt.objects.filter(no_orden=no_orden).order_by('item')
    
    b = ActividadesTecnico.objects.filter(no_orden=no_orden)
    try:
        LogCliente.objects.create(
            id_hd=c[0].id_hd,
            no_orden=no_orden,
            fecha_hora_visita=datetime.now(),
        )
    except Exception as e:
        print(e)
    try:
        PushInformation.objects.get(user=request.user)
        context['user_have_push'] = True
    except Exception as e:
        print(e)
        context['user_have_push'] = False
    try:
        #items_list = b.values_list('item', flat=True)
        context['filas'] = []
        for row in a:
            context['filas'].append(
                {
                    "item": b.get(item=row.item).item,
                    "estado": b.get(item=row.item).estado,
                    "refaccion": row.refaccion,
                    "precio": row.total,
                }
            )
        #context['buen_estado'] = b.filter(estado='Buen Estado').count()
        context['recomendado'] = b.filter(estado='Recomendado').count()
        context['urgente'] = b.filter(estado='Inmediato').count()
        orden = VOperacionesRefacciones.objects.filter(no_orden=no_orden)[0]
        context['orden'] = orden
        ev = VSeguimientoEvidencia.objects.filter(no_orden=no_orden)
        #context['filas_media'] = []
        #context['filas_video'] = []
        #media = ActividadesTecnicoMedios.objects.filter(no_orden=no_orden)
        '''
        for file in media:
            file_type = mimetypes.guess_type(file.evidencia)[0]
            if 'video' in file_type:
                context['filas_video'].append(file)
            else:
                context['filas_media'].append(file)
        '''
        context['recomendado_ev'] = ev.filter(estado='Recomendado')
        context['urgente_ev'] = ev.filter(estado='Inmediato')
        #context['pdf'] = ActividadesRefaccionesCotizacionPdf.objects.get(
        #    no_orden=no_orden)
        #pdffile = os.path.join(settings.MEDIA_ROOT, context['pdf'].nombre_pdf)
        #doc = fitz.open(pdffile)
        #context['fotos'] = []
        #for i in range(doc.pageCount):
        #    context['fotos'].append(f'{no_orden}{i}.png')
    except Exception as e:
        print(e)
    if request.is_ajax():
        r = request.POST
        try:
            ActividadesCliente.objects.get(no_orden=no_orden, refaccion=r['refaccion'])
        except Exception as e:
            print(e)
            ActividadesCliente.objects.create(
                no_orden=orden.no_orden,
                placas=orden.placas,
                item=r['item'],
                refaccion=r['refaccion'],
                autorizacion=r.get('autorizacion', 'No Autorizado'),
                fecha_hora_autorizacion=datetime.now(),
            )
    return render(request, 'seguimientolite/cotizacion.html', context)


def configuracion(request):
    if not request.user.is_authenticated:
        return redirect('/asesor/')
    return render(request, 'seguimientolite/configuracion.html')


def pdf_camiones(request):
    return render(request)


def recepcionactiva_pdf(request, no_orden):
    queryset_items = ActividadesAsesorRecepcionActiva.objects.filter(no_orden=no_orden)
    queryset_info = VOperacionesRecepcionActiva.objects.filter(noorden=no_orden)[0].get()
    filepdf = get_pdf(queryset_items, queryset_info)
    return FileResponse(filepdf, as_attachment=False, filename='recepcionactivasos.pdf')


def calidad_pdf(request, no_orden):
    try:
        info = VOperacionesRefacciones.objects.filter(no_orden=no_orden)[0]
        lista_calidad = ListaItemsCalidad.objects.exclude(familia='Complementarias')
        items_tecnico = ActividadesTecnico.objects.filter(no_orden=no_orden)
    except Exception as e:
        print(e)
    try:
        calidad = ActividadesTecnicoCalidad.objects.get(no_orden=no_orden)
    except Exception as e:
        print(e)
        calidad = None
    try:
        items_calidad = ActividadesCalidad.objects.filter(no_orden=no_orden)
    except Exception as e:
        print(e)
        items_calidad = None
    try:
        firma = ActividadesAsesorFirmas.objects.get(no_orden=no_orden).firma
    except Exception as e:
        print(e)
        firma = None
    buffer = get_pdf_calidad(items_tecnico, items_calidad, lista_calidad, firma, calidad, info)
    return FileResponse(buffer, as_attachment=False, filename=f'calidad_{no_orden}.pdf')



@require_POST
def upload(request):
    image = upload_receive(request)
    print(image)

    instance = Photo(image=image)
    instance.save()

    basename = os.path.basename(instance.image.path)

    file_dict = {
        'name': basename,
        'size': image.size,

        'url': settings.MEDIA_URL + basename,
        'thumbnailUrl': settings.MEDIA_URL + basename,
        'deleteType': 'POST'
    }
    return UploadResponse(request, file_dict)


@require_POST
def upload_delete(request, pk):
    success = True
    try:
        instance = Photo.objects.get(pk=pk)
        os.unlink(instance.file.path)
        instance.delete()
    except Photo.DoesNotExist:
        success = False

    return JFUResponse(request, success)
