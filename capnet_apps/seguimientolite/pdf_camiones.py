import os
from django.conf import settings
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Paragraph, Table, TableStyle
from reportlab.lib.enums import TA_JUSTIFY, TA_LEFT, TA_CENTER
from reportlab.lib import colors
from datetime import datetime


def get_pdf(queryset_items, queryset_calidad):
    width, height = A4
    styles = getSampleStyleSheet()
    styleN = styles["BodyText"]
    styleN.alignment = TA_LEFT
    styleBH = styles["Normal"]
    styleBH.alignment = TA_CENTER

    def coord(x, y, unit=1):
        x, y = x * unit, height -  y * unit
    return x, y

    hDesc= Paragraph('''<b>Descripción</b>''', styleBH)
    hMO= Paragraph('''<b>Mano de Obra</b>''', styleBH)
    hRef = Paragraph('''<b>Refacciones</b>''', styleBH)
    hTotal = Paragraph('''<b>Total</b>''', styleBH)

    data= [[hDesc, hMO, hRef, hTotal]]

    for query in queryset_items:
        row = []
        row.append(Paragraph(query.item, styleN))
        row.append(Paragraph(query.mo, styleN))
        row.append(Paragraph(query.refacciones, styleN))
        row.append(Paragraph(query.total, styleN))
        data.append(row)
       
    table = Table(data, colWidths=[11.5* cm, 2.5 * cm, 2.7 * cm, 2.5* cm])

    table.setStyle(TableStyle([
        ('INNERGRID', (0,0), (-1,-1), 0.25, colors.white),
        ('BOX', (0,0), (-1,-1), 0.25, colors.black),
        ('VALIGN',(0,0),(-1,-1),'TOP'),
    ]))
    filename = f"RECEPCION_ACTIVA_{queryset_info.no_orden}.pdf"
    file_dir = os.path.join(settings.MEDIA_ROOT, filename)
    buffer = io.BytesIO()
    ##c = canvas.Canvas(file_dir, pagesize=A4)
    c = canvas.Canvas(buffer)
    table.wrapOn(c, width, height)
    table.drawOn(c, *coord(1, 13.0, cm))
    #_________________________________________________________________________________#
    no_orden= queryset_info.no_orden
    fecha = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    asesor = queryset_info.asesor
    tecnico= queryset_info.tecnico
    cliente = queryset_info.cliente
    telefono= '322 4568'
    email = 'eliu.hepher@capitalnetwork.com.mx'
    modelo = 'Modelo de prueba'
    VIN = queryset_info.vin
    placa = queryset_info.placas
    km= '133546'
    color = 'Blanco'
    #------------------------------------------------------------------------------------#
    c.setFont('Helvetica', 9)
    # Dibujamos texto: (X,Y,Texto)
    c.drawString(25,780,f"Número de Presupuesto: {no_orden}")
    c.drawString(25,765,f"Fecha de Emisión: {fecha}")
    c.drawString(360,765,"MAQUINARIAS Y VEHICULOS S.A. MAVESA")

    c.drawString(335,750,"Av. Juan Tanca Marengo Km.3 1/2 y Las Aguas, Guayaquil")
    c.drawString(25,750,f"Asesor: {asesor}")
    c.drawString(25,735,f"Técnico: {tecnico}")
    c.setFont('Helvetica-Bold', 9)

    c.drawString(222,700, "Presupuestos adicionales")
    c.setFont('Helvetica', 9)
    c.drawString(25,680,f"Cliente: {cliente}")
    c.drawString(25,668,f"Teléfono: {telefono}")
    c.drawString(25,656,f"Email: {email}")
    c.drawString(288,680,f"VIN: {VIN}")
    c.drawString(288,668,f"Placa: {placa}")
    c.drawString(288,656,f"Modelo: {modelo}")
    c.drawString(490,680,f"Color: {color}")
    c.drawString(490,668,f"Kilometraje: {km}")
    c.setFont('Helvetica-Bold', 9)
    c.drawString(229,600, "Adicionales Autorizados")
    c.setFont('Helvetica', 9)
    # Dibujamos una imagen (IMAGEN, X,Y, WIDTH, HEIGH)
    c.drawImage('hino_mavesa.jpg', 340, 780, 200, 28)
    c.drawImage('pie_mavesa.jpg', 20, 40, 550, 35)
    c.save()
    buffer.seek(0)
    return buffer