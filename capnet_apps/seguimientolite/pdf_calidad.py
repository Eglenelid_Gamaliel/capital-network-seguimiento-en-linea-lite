""" Container of PDF operations """
import os
import io
import xml.etree.ElementTree as ET
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from PyPDF2 import PdfFileWriter, PdfFileReader
from django.conf import settings

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def get_pdf_calidad(queryset_tecnico, queryset_calidad, lista_calidad, firma, calidad, info):
    buffer = io.BytesIO()
    # Create a new PDF with Reportlab
    can = canvas.Canvas(buffer, pagesize=letter)
    can.setFont('Helvetica-Bold', 10)

    caracter_default = "✓"
    caracter_no_default="x"
    km = "72340"

    try:
        for row in lista_calidad.exclude(familia='Prueba de Ruta'):
            try:
                queryset_tecnico.get(item=row.descripcion)
                can.drawString(row.cord_no_x, row.cord_no_y, caracter_no_default)
            except Exception:
                can.drawString(row.cord_si_x, row.cord_si_y, caracter_default)
        for row in lista_calidad.filter(familia='Prueba de Ruta'):
            try:
                reporte = queryset_calidad.get(item=row.descripcion)
                if reporte.estado == 'Malo':
                    can.drawString(row.cord_no_x, row.cord_no_y, caracter_no_default)
                else:
                    can.drawString(row.cord_si_x, row.cord_si_y, caracter_default)
            except Exception:
                can.drawString(row.cord_si_x, row.cord_si_y, caracter_default)
        try:
            can.drawImage(os.path.join(settings.MEDIA_ROOT, firma) , 357, 40, 90, 45, mask='auto')
        except Exception as e:
            print(e)

        try:
            can.drawString(370, 335, calidad.presion_del_derecha)#presion delantera derecha
            can.drawString(430, 335, calidad.presion_del_izquierda)#presion delantera izq
            can.drawString(370, 318, calidad.presion_tras_derecha)#presion trasera derecha
            can.drawString(430, 318, calidad.presion_tras_izquierda)#presion trasera izq
            can.drawString(400, 300, calidad.presion_refaccion)#presion repuesto
            if calidad.estado_tras_derecha == '25':
                can.drawString(295, 420, caracter_no_default)#desgaste tras derecha
            elif calidad.estado_tras_derecha == '50':
                can.drawString(310, 420, caracter_no_default)#
            elif calidad.estado_tras_derecha == '75':
                can.drawString(322, 420, caracter_no_default)#
            elif calidad.estado_tras_derecha == '100':
                can.drawString(337, 420, caracter_no_default)#

            if calidad.estado_del_derecha == '25':
                can.drawString(415, 420, caracter_no_default)#desgaste delantera derecha
            elif calidad.estado_del_derecha == '50':
                can.drawString(430, 420, caracter_no_default)#
            elif calidad.estado_del_derecha == '75':
                can.drawString(442, 420, caracter_no_default)#
            elif calidad.estado_del_derecha == '100':
                can.drawString(457, 420, caracter_no_default)#

            if calidad.estado_del_izquierda == '25':
                can.drawString(295, 370, caracter_no_default)#desgaste delantera izq
            elif calidad.estado_del_izquierda == '50':
                can.drawString(310, 370, caracter_no_default)#
            elif calidad.estado_del_izquierda == '75':
                can.drawString(322, 370, caracter_no_default)#
            elif calidad.estado_del_izquierda == '100':
                can.drawString(337, 370, caracter_no_default)#

            if calidad.estado_tras_izquierda == '25':
                can.drawString(415, 370, caracter_no_default)#desgaste trasera izq
            elif calidad.estado_tras_izquierda == '50':
                can.drawString(430, 370, caracter_no_default)#
            elif calidad.estado_tras_izquierda == '75':
                can.drawString(442, 370, caracter_no_default)#
            elif calidad.estado_tras_izquierda == '100':
                can.drawString(457, 370, caracter_no_default)#
        except Exception as e:
            print(e)
        '''
        info cabeceras
        '''
        can.drawString(370, 495, info.vehiculo)#
        can.drawImage(os.path.join(settings.MEDIA_ROOT, 'firma-2.png'), 706, 0, 120, 75, mask='auto')
        

        '''
        FIRMA ASESOR
        '''
        if info.asesor == 'ENRIQUE TROYA PALMA':
            can.drawImage(os.path.join(settings.BASE_DIR, 'firmas citroenasesor', 'ENRIQUE TROYA.png') , 83, 45, 54, 61, mask='auto')
        elif info.asesor == 'SAKIA VIDEAUX':
            can.drawImage(os.path.join(settings.BASE_DIR, 'firmas citroenasesor', 'SAKIA VIDEAUX.png') , 83, 45, 58, 49, mask='auto')
        elif info.asesor == 'JOHANNA YELA OLVERA':
            can.drawImage(os.path.join(settings.BASE_DIR, 'firmas citroenasesor', 'JOHANNA YELA.png') , 83, 45, 79, 38, mask='auto')


    except Exception as e:
        print(e)
    can.showPage()
    can.save()
    # Ir al inicio del  StringIO buffer
    buffer.seek(0)
    new_pdf = PdfFileReader(buffer)
    # Leer el pdf existente
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    my_file = os.path.join(THIS_FOLDER, 'CITROEN.pdf')
    existing_pdf = PdfFileReader(open(my_file, "rb"))
    output = PdfFileWriter()
    # La "Marca de agua" es el pdf de plantilla
    page = existing_pdf.getPage(0)
    
    page.mergePage(new_pdf.getPage(0))
    
    output.addPage(page)
    buffer_pdf = io.BytesIO()
    output.write(buffer_pdf)
    buffer_pdf.seek(0)
    # escribe en la ruta especificada
    #output_stream = open(f"./{queryset_tecnico[0].no_orden}" + ".pdf", "wb")
    #output.write(output_stream)
    #output_stream.close()
    return buffer_pdf
