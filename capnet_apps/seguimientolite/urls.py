# chat/urls.py
from django.urls import path, include
from . import views

urlpatterns = [
    path('recepcionactiva/cotizacion/<str:id_hd>/',
        views.recepcionactiva_cotizacion, name='cliente login'),
    path('recepcionactiva/detalle/<str:id_hd>/',
        views.recepcionactiva_detalle, name='recepcion_activa_detalle'),
    path('recepcionactiva/<str:placas>/',
        views.recepcionactiva, name='recepcion_activa'),
    path('get-pdf/<str:no_orden>', views.recepcionactiva_pdf, name='recepcion_activa_pdf'),
    path('calidad_pdf/<str:no_orden>', views.calidad_pdf, name='calidad_pdf'),
    path('asesor/', views.asesor, name='ordenes'),
    path('asesor/<str:no_orden>/', views.asesor_detalle, name='detalle_ordenes'),
    path('refacciones/', views.refecciones, name='refacciones'),
    path('refacciones/<str:no_orden>/',
        views.refacciones_detalle, name='refacciones_detalle'),
    path('calidad', views.calidad, name="calidad"),
    path('calidad/<str:no_orden>/', views.calidad_detalle, name="calidad_detalle"),
    path('tecnico/<str:id_tecnico>/<str:no_orden>/',
        views.tecnico, name="tecnico"),
    path('cotizacion/<str:no_orden>/',
        views.cotizacion_cliente, name='detalle_ordenes'),
    path('cliente/<str:no_orden>/', views.client_autologin, name='cliente login'),
    path('jefedetaller/', views.jefe_de_taller, name='jefe_de_taller'),
    path('jefedetaller/<str:no_orden>/',
        views.jefe_de_taller_detalle, name='jefe_de_taller'),
    path('login/', views.staff_login, name='staff_login'),
    path('configuracion/', views.configuracion, name='configuracion'),
    path('upload/', views.upload, name='jfu_upload'),
]
