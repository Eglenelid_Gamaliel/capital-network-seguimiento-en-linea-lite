from django.db import models
from django import forms
from django.conf import settings


class Photo(models.Model):
    image = models.ImageField()


class Audio(models.Model):
    sound = models.FileField()


class VOperacionesTecnico(models.Model):
    id = models.BigIntegerField(blank=True, null=False, primary_key=True)
    id_hd = models.DecimalField(
        max_digits=18, decimal_places=0, blank=True, null=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    vin = models.CharField(max_length=50, blank=True, null=True)
    placas = models.CharField(max_length=10, blank=True, null=True)
    tecnico = models.CharField(max_length=60, blank=True, null=True)
    asesor = models.CharField(max_length=150, blank=True, null=True)
    vehiculo = models.CharField(max_length=250, blank=True, null=True)
    fecha_llegada = models.DateField(blank=True, null=True)
    hora_promesa = models.DateTimeField(blank=True, null=True)
    cliente = models.CharField(max_length=200, blank=True, null=True)
    servicio_capturado = models.CharField(
        max_length=500, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_operaciones_tecnico'


class ActividadesTecnico(models.Model):
    id = models.BigAutoField(primary_key=True)
    tecnico = models.CharField(max_length=50, blank=True, null=True)
    id_hd = models.BigIntegerField(blank=True, null=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    item = models.CharField(max_length=100, blank=True, null=True)
    estado = models.CharField(max_length=25, blank=True, null=True)
    comentarios = models.CharField(max_length=500, blank=True, null=True)
    fecha_hora_inicio = models.DateTimeField()
    fecha_hora_fin = models.DateTimeField()
    fecha_hora_actualizacion = models.DateTimeField(null=True)
    num_tor = models.CharField(max_length=20, blank=True, null=True)
    valor = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'actividades_tecnico'


class ActividadesTecnicoMedios(models.Model):
    id = models.BigAutoField(primary_key=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    item = models.CharField(max_length=100, blank=True, null=True)
    evidencia = models.CharField(max_length=100)
    fecha_hora_subida = models.DateTimeField()

    class Meta:
        managed = True
        db_table = 'actividades_tecnico_medios'


class VOperacionesRefacciones(models.Model):
    fecha_ingreso = models.DateField(blank=True, null=True)
    no_orden = models.CharField(max_length=25, blank=True, primary_key=True)
    placas = models.CharField(max_length=10, blank=True, null=True)
    vin = models.CharField(max_length=50, blank=True, null=True)
    vehiculo = models.CharField(max_length=250, blank=True, null=True)
    asesor = models.CharField(max_length=150, blank=True, null=True)
    tecnico = models.CharField(max_length=60, blank=True, null=True)
    #num_tor = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_operaciones_refacciones'


class ActividadesRefaccionesCotizacionPdf(models.Model):
    id = models.BigAutoField(primary_key=True)
    no_orden = models.CharField(max_length=50, blank=True, null=True)
    fecha_hora_inicio = models.DateTimeField(blank=True, null=True)
    fecha_hora_fin = models.DateTimeField(blank=True, null=True)
    nombre_pdf = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'actividades_refacciones_cotizacion_pdf'


class VOperacionesAsesor(models.Model):
    fecha_ingreso = models.DateField(blank=True, null=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    placas = models.CharField(max_length=10, blank=True, null=True)
    vin = models.CharField(max_length=50, blank=True, null=True)
    vehiculo = models.CharField(max_length=250, blank=True, null=True)
    asesor = models.CharField(max_length=150, blank=True, null=True)
    #tecnico = models.CharField(max_length=60, blank=True, null=True)
    fecha_hora_fin_refacciones = models.DateTimeField(blank=True, null=True)
   # num_tor = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_operaciones_asesor'


class VSeguimientoEvidencia(models.Model):
    id = models.BigIntegerField(primary_key=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    item = models.CharField(max_length=100, blank=True, null=True)
    tecnico = models.CharField(max_length=50, blank=True, null=True)
    estado = models.CharField(max_length=25, blank=True, null=True)
    comentarios = models.CharField(max_length=500, blank=True, null=True)
    fecha_hora_inicio = models.DateTimeField(blank=True, null=True)
    fecha_hora_fin = models.DateTimeField(blank=True, null=True)
    fecha_hora_actualizacion = models.DateTimeField(blank=True, null=True)
    evidencia = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_seguimiento_evidencia'


class VOperacionesRecepcionActiva(models.Model):
    id_hd = models.AutoField(primary_key=True)
    numcita = models.CharField(max_length=25)
    noorden = models.CharField(max_length=25)
    horallegada = models.DateTimeField(blank=True, null=True)
    noplacas = models.CharField(db_column='noPlacas', max_length=10, blank=True, null=True)  # Field name made lowercase.
    asesor = models.CharField(max_length=150, blank=True, null=True)
    cliente = models.CharField(db_column='Cliente', max_length=200, blank=True, null=True)  # Field name made lowercase.
    tipocliente = models.CharField(max_length=25, blank=True, null=True)
    vehiculo = models.CharField(db_column='Vehiculo', max_length=250, blank=True, null=True)  # Field name made lowercase.
    vin = models.CharField(max_length=50, blank=True, null=True)
    colorprisma = models.CharField(db_column='colorPrisma', max_length=25, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_operaciones_recepcion_activa'


class Tecnicos(models.Model):
    id_empleado = models.CharField(db_column='ID_EMPLEADO', max_length=10)  # Field name made lowercase.
    id_tipo_empleado = models.CharField(db_column='ID_TIPO_EMPLEADO', max_length=10, blank=True, null=True)  # Field name made lowercase.
    nombre_empleado = models.CharField(db_column='NOMBRE_EMPLEADO', max_length=60, blank=True, null=True)  # Field name made lowercase.
    nivel = models.CharField(db_column='NIVEL', max_length=1, blank=True, null=True)  # Field name made lowercase.
    bahia = models.IntegerField(db_column='BAHIA', blank=True, null=True)  # Field name made lowercase.
    express = models.BooleanField(db_column='EXPRESS', blank=True, null=True)  # Field name made lowercase.
    color_tecnico = models.CharField(db_column='COLOR_TECNICO', max_length=25, blank=True, null=True)  # Field name made lowercase.
    hora_ent_lv = models.CharField(db_column='HORA_ENT_LV', max_length=5, blank=True, null=True)  # Field name made lowercase.
    hora_sal_lv = models.CharField(db_column='HORA_SAL_LV', max_length=5, blank=True, null=True)  # Field name made lowercase.
    hora_comer = models.CharField(db_column='HORA_COMER', max_length=5, blank=True, null=True)  # Field name made lowercase.
    hora_ent_s = models.CharField(db_column='HORA_ENT_S', max_length=5, blank=True, null=True)  # Field name made lowercase.
    hora_sal_s = models.CharField(db_column='HORA_SAL_S', max_length=5, blank=True, null=True)  # Field name made lowercase.
    id_asesor = models.CharField(db_column='ID_ASESOR', max_length=10, blank=True, null=True)  # Field name made lowercase.
    nombre_asesor = models.CharField(db_column='NOMBRE_ASESOR', max_length=50, blank=True, null=True)  # Field name made lowercase.
    no_emp_asesor = models.IntegerField(db_column='NO_EMP_ASESOR', blank=True, null=True)  # Field name made lowercase.
    jefe_taller = models.BooleanField(db_column='JEFE_TALLER', blank=True, null=True)  # Field name made lowercase.
    hora_ent_d = models.CharField(db_column='HORA_ENT_D', max_length=5, blank=True, null=True)  # Field name made lowercase.
    hora_sal_d = models.CharField(db_column='HORA_SAL_D', max_length=5, blank=True, null=True)  # Field name made lowercase.
    hora_comer_s = models.CharField(db_column='HORA_COMER_S', max_length=5, blank=True, null=True)  # Field name made lowercase.
    hora_comer_d = models.CharField(db_column='HORA_COMER_D', max_length=5, blank=True, null=True)  # Field name made lowercase.
    id_empleado_bi = models.CharField(db_column='ID_EMPLEADO_BI', max_length=10, blank=True, null=True)  # Field name made lowercase.
    min_comer_lv = models.IntegerField(db_column='MIN_COMER_LV', blank=True, null=True)  # Field name made lowercase.
    min_comer_s = models.IntegerField(db_column='MIN_COMER_S', blank=True, null=True)  # Field name made lowercase.
    min_comer_d = models.IntegerField(db_column='MIN_COMER_D', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'tecnicos'


class ActividadesTecnicoMediosRecepcionActiva(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_hd = models.BigIntegerField(blank=True, null=True)
    placas = models.CharField(max_length=25, blank=True, null=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    item = models.CharField(max_length=100, blank=True, null=True)
    evidencia = models.CharField(max_length=100)
    fecha_hora_subida = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'actividades_tecnico_medios_recepcion_activa'


class ActividadesTecnicoRecepcionActiva(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_hd = models.BigIntegerField(blank=True, null=True)
    placas = models.CharField(max_length=25, blank=True, null=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    item = models.CharField(max_length=100, blank=True, null=True)
    tecnico = models.CharField(max_length=50, blank=True, null=True)
    estado = models.CharField(max_length=25, blank=True, null=True)
    comentarios = models.CharField(max_length=500, blank=True, null=True)
    fecha_hora_inicio = models.DateTimeField(blank=True, null=True)
    fecha_hora_fin = models.DateTimeField(blank=True, null=True)
    fecha_hora_actualizacion = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'actividades_tecnico_recepcion_activa'

class VOperacionesAsesorRecepcionActiva(models.Model):
    fecha_ingreso = models.DateTimeField(blank=True, null=True)
    id_hd = models.DecimalField(primary_key=True, max_digits=18, decimal_places=0)
    cliente = models.CharField(max_length=200, blank=True, null=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    placas = models.CharField(max_length=10, blank=True, null=True)
    vin = models.CharField(max_length=50, blank=True, null=True)
    vehiculo = models.CharField(max_length=250, blank=True, null=True)
    asesor = models.CharField(max_length=150, blank=True, null=True)
    tecnico = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_operaciones_asesor_recepcion_activa'


class ActividadesAsesorRecepcionActiva(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_hd = models.BigIntegerField(blank=True, null=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    placas = models.CharField(max_length=25, blank=True, null=True)
    vin = models.CharField(max_length=25, blank=True, null=True)
    cliente = models.CharField(max_length=50, blank=True, null=True)
    vehiculo = models.CharField(max_length=50, blank=True, null=True)
    asesor = models.CharField(max_length=50, blank=True, null=True)
    tecnico = models.CharField(max_length=50, blank=True, null=True)
    item = models.CharField(max_length=50, blank=True, null=True)
    estado = models.CharField(max_length=50, blank=True, null=True)
    comentarios = models.CharField(max_length=200, blank=True, null=True)
    mo = models.DecimalField(max_digits=18, decimal_places=0, blank=True, null=True)
    refacciones = models.DecimalField(max_digits=18, decimal_places=0, blank=True, null=True)
    total = models.DecimalField(max_digits=18, decimal_places=0, blank=True, null=True)
    fecha_hora_guardado = models.DateTimeField(blank=True, null=True)
    fecha_hora_modificacion = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'actividades_asesor_recepcion_activa'


class ActividadesCliente(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_hd = models.BigIntegerField(blank=True, null=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    placas = models.CharField(max_length=25, blank=True, null=True)
    item = models.CharField(max_length=50, blank=True, null=True)
    refaccion = models.CharField(max_length=50, blank=True, null=True)
    estado = models.CharField(max_length=50, blank=True, null=True)
    autorizacion = models.CharField(max_length=50, blank=True, null=True)
    fecha_hora_autorizacion = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'actividades_cliente'


class VInterfazCotizacionesMo(models.Model):
    document_type = models.IntegerField(db_column='document type')  # Field renamed to remove unsuitable characters.
    vin = models.CharField(max_length=20)
    no_orden = models.CharField(max_length=30, blank=True, null=True)
    no_field = models.CharField(db_column='no_', max_length=20)  # Field renamed because it ended with '_'.
    description = models.CharField(db_column='Description', max_length=50)  # Field name made lowercase.
    description_2 = models.CharField(db_column='Description 2', max_length=50)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    quantity = models.DecimalField(db_column='Quantity', max_digits=38, decimal_places=20)  # Field name made lowercase.
    unit_price = models.DecimalField(db_column='Unit price', max_digits=38, decimal_places=20)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amount = models.DecimalField(max_digits=38, decimal_places=20)
    amount_including_vat = models.DecimalField(db_column='Amount including VAT', max_digits=38, decimal_places=20)  # Field name made lowercase. Field renamed to remove unsuitable characters.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_interfaz_cotizaciones_MO'


class VInterfazCotizacionesRef(models.Model):
    id = models.BigIntegerField(primary_key=True)
    vin = models.CharField(db_column='VIN', max_length=20)  # Field name made lowercase.
    document_type = models.IntegerField(db_column='document type')  # Field renamed to remove unsuitable characters.
    no_orden = models.CharField(max_length=30, blank=True, null=True)
    no_field = models.CharField(db_column='no_', max_length=20)  # Field renamed because it ended with '_'.
    description = models.CharField(db_column='Description', max_length=50)  # Field name made lowercase.
    description_2 = models.CharField(db_column='Description 2', max_length=50)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    quantity = models.DecimalField(db_column='Quantity', max_digits=38, decimal_places=20)  # Field name made lowercase.
    unit_price = models.DecimalField(db_column='Unit price', max_digits=38, decimal_places=20)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    amount = models.DecimalField(max_digits=38, decimal_places=20)
    amount_including_vat = models.DecimalField(db_column='Amount including VAT', max_digits=38, decimal_places=20)  # Field name made lowercase. Field renamed to remove unsuitable characters.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_interfaz_cotizaciones_ref'


class VInterfazCotizacionesTotal(models.Model):
    no_orden = models.CharField(max_length=30, blank=True, null=True)
    monto_total_cotización = models.DecimalField(max_digits=38, decimal_places=20, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_interfaz_cotizaciones_total'


class VUsuarios(models.Model):
    cvegrupo = models.IntegerField(db_column='cveGrupo')  # Field name made lowercase.
    cveperfil = models.IntegerField(db_column='cvePerfil')  # Field name made lowercase.
    cveusuario = models.CharField(db_column='cveUsuario', max_length=15)  # Field name made lowercase.
    pass_field = models.CharField(db_column='Pass', max_length=100)  # Field name made lowercase. Field renamed because it was a Python reserved word.
    nombre = models.CharField(db_column='Nombre', max_length=150, blank=True, null=True)  # Field name made lowercase.
    correoe = models.CharField(db_column='correoE', max_length=100, blank=True, null=True)  # Field name made lowercase.
    color = models.CharField(db_column='Color', max_length=50, blank=True, null=True)  # Field name made lowercase.
    cveasesor = models.CharField(db_column='cveAsesor', max_length=20, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_usuarios'


class LogCliente(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_hd = models.BigIntegerField(blank=True, null=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    cliente = models.BinaryField(blank=True, null=True)
    fecha_hora_visita = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'log_cliente'


class LogEnvios(models.Model):
    id = models.BigAutoField(primary_key=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    medio = models.CharField(max_length=50, blank=True, null=True)
    fecha_hora_envio = models.DateTimeField(blank=True, null=True)
    telefono = models.CharField(max_length=50, blank=True, null=True)
    correo = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'log_envios'


class ActividadesRefaccionesAlt(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_hd = models.BigIntegerField(blank=True, null=True)
    no_orden = models.CharField(max_length=50, blank=True, null=True)
    item = models.CharField(max_length=50, blank=True, null=True)
    cantidad = models.IntegerField(blank=True, null=True)
    descripcion = models.CharField(max_length=50, blank=True, null=True)
    no_parte = models.CharField(max_length=50, blank=True, null=True)
    nombre = models.CharField(max_length=50, blank=True, null=True)
    precio = models.DecimalField(max_digits=18, decimal_places=2, blank=True, null=True)
    precio_iva = models.DecimalField(max_digits=18, decimal_places=2, blank=True, null=True)
    existencia = models.CharField(max_length=10, blank=True, null=True)
    localizacion = models.CharField(max_length=50, blank=True, null=True)
    fecha_hora_fin = models.DateTimeField(blank=True, null=True)


    class Meta:
        managed = True
        db_table = 'actividades_refacciones_alt'


class ActividadesAsesorAlt(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_hd = models.BigIntegerField(blank=True, null=True)
    no_orden = models.CharField(max_length=50, blank=True, null=True)
    placas = models.CharField(max_length=50, blank=True, null=True)
    vin = models.CharField(max_length=50, blank=True, null=True)
    item = models.CharField(max_length=50, blank=True, null=True)
    refaccion = models.CharField(max_length=200, blank=True, null=True)
    precio_iva = models.DecimalField(max_digits=18, decimal_places=2, blank=True, null=True)
    existencia = models.CharField(max_length=50, blank=True, null=True)
    localizacion = models.CharField(max_length=50, blank=True, null=True)
    garantia_seguro = models.CharField(max_length=50, blank=True, null=True)
    rvt = models.CharField(db_column='RVT', max_length=50, blank=True, null=True)  # Field name made lowercase.
    cantidad_ut = models.DecimalField(max_digits=18, decimal_places=2, blank=True, null=True)
    mo = models.DecimalField(max_digits=18, decimal_places=2, blank=True, null=True)
    mo_iva = models.DecimalField(max_digits=18, decimal_places=2, blank=True, null=True)
    total = models.DecimalField(max_digits=18, decimal_places=2, blank=True, null=True)
    descuento_mo = models.CharField(max_length=50, blank=True, null=True)
    descuento_ref = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'actividades_asesor_alt'


class LogGeneral(models.Model):
    id = models.BigAutoField(primary_key=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    inicio_tecnico = models.DateTimeField(blank=True, null=True)
    fin_tecnico = models.DateTimeField(blank=True, null=True)
    inicio_refacciones = models.DateTimeField(blank=True, null=True)
    fin_refacciones = models.DateTimeField(blank=True, null=True)
    inicio_jdt = models.DateTimeField(blank=True, null=True)
    fin_jdt = models.DateTimeField(blank=True, null=True)
    inicio_asesor = models.DateTimeField(blank=True, null=True)
    fin_asesor = models.DateTimeField(blank=True, null=True)
    

    class Meta:
        managed = True
        db_table = 'log_general'


class LogRecepcionActiva(models.Model):
    id = models.BigAutoField(primary_key=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    inicio_recepcion = models.DateTimeField(blank=True, null=True)
    fin_recepcion = models.DateTimeField(blank=True, null=True)
    inicio_cotizacion = models.DateTimeField(blank=True, null=True)
    fin_cotizacion = models.DateTimeField(blank=True, null=True)
    

    class Meta:
        managed = True
        db_table = 'log_recepcion_activa'
        
class VOperacionesAsesorAlt(models.Model):
    #id = models.BigIntegerField(primary_key=True)
    #fecha_ingreso = models.DateField(blank=True, null=True)
    no_orden = models.CharField(max_length=25, primary_key=True)
    placas = models.CharField(max_length=10, blank=True, null=True)
    vin = models.CharField(max_length=50, blank=True, null=True)
    vehiculo = models.CharField(max_length=250, blank=True, null=True)
    asesor = models.CharField(max_length=150, blank=True, null=True)
    #fecha_hora_fin_refacciones = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_operaciones_asesor_alt'


class ListaItemsCalidad(models.Model):
    id = models.BigAutoField(primary_key=True)
    descripcion = models.CharField(max_length=250, blank=True, null=True)
    familia = models.CharField(max_length=50, blank=True, null=True)
    cord_si_x = models.IntegerField(blank=True, null=True)
    cord_si_y = models.IntegerField(blank=True, null=True)
    cord_no_x = models.IntegerField(blank=True, null=True)
    cord_no_y = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'lista_items_calidad'


class ActividadesCalidad(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_hd = models.CharField(max_length=25, blank=True, null=True)
    no_orden = models.CharField(max_length=25, blank=True, null=True)
    item = models.CharField(max_length=100, blank=True, null=True)
    estado = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'actividades_calidad'


class ActividadesAsesorFirmas(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_hd = models.CharField(max_length=50, blank=True, null=True)
    no_orden = models.CharField(max_length=50, blank=True, null=True)
    firma = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'actividades_asesor_firmas'


class ActividadesTecnicoCalidad(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_hd = models.CharField(max_length=50, blank=True, null=True)
    no_orden = models.CharField(max_length=50, blank=True, null=True)
    presion_del_izquierda = models.CharField(max_length=50, blank=True, null=True)
    presion_del_derecha = models.CharField(max_length=50, blank=True, null=True)
    presion_tras_izquierda = models.CharField(max_length=50, blank=True, null=True)
    presion_tras_derecha = models.CharField(max_length=50, blank=True, null=True)
    presion_refaccion = models.CharField(max_length=50, blank=True, null=True)
    estado_del_izquierda = models.CharField(max_length=50, blank=True, null=True)
    estado_del_derecha = models.CharField(max_length=50, blank=True, null=True)
    estado_tras_izquierda = models.CharField(max_length=50, blank=True, null=True)
    estado_tras_derecha = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'actividades_tecnico_calidad'