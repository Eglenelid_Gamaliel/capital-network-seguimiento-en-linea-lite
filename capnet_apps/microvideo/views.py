import os
from django.conf import settings
from django.urls import reverse
from django.views import generic
from django.views.decorators.http import require_POST
from django.shortcuts import render
from django.forms.models import model_to_dict
from jfu.http import upload_receive, UploadResponse, JFUResponse
import mimetypes
from .models import media


def home(request, id_hd, origin):
    context = {}
    context['id_hd'] = id_hd
    context['origin'] = origin
    if request.method == 'POST':
        image = upload_receive(request)
        print(image)

        instance = media(name=image, id_hd=id_hd, origin=origin)
        instance.save()

        print(model_to_dict(instance))

        basename = os.path.basename(instance.name.path)
        print(basename)
        file_dict = {
            'name': basename,
            'size': image.size,

            'url': settings.MEDIA_URL + basename,
            'thumbnailUrl': settings.MEDIA_URL + basename,
            'deleteType': 'POST'
        }
        return UploadResponse(request, file_dict)
    return render(request, 'microvideo/media_upload.html', context)


@require_POST
def upload(request):
    image = upload_receive(request)
    print(image)

    instance = media(name=image)
    instance.save()

    basename = os.path.basename(instance.name.path)

    file_dict = {
        'name': basename,
        'size': image.size,

        'url': settings.MEDIA_URL + basename,
        'thumbnailUrl': settings.MEDIA_URL + basename,
        'deleteType': 'POST'
    }
    return UploadResponse(request, file_dict)


@require_POST
def upload_delete(request, pk):
    success = True
    try:
        instance = Photo.objects.get(pk=pk)
        os.unlink(instance.file.path)
        instance.delete()
    except Photo.DoesNotExist:
        success = False

    return JFUResponse(request, success)
