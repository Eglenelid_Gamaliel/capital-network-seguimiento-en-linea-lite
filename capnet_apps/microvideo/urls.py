from django.urls import path, include
from . import views

urlpatterns = [
    path('upload/', views.upload, name='media_upload'),
    path('<str:origin>/<str:id_hd>/', views.home, name='home')
]
