from django.db import models

# Create your models here.


class media(models.Model):
    id = models.AutoField(primary_key=True)
    id_hd = models.CharField(max_length=30, null=True)
    name = models.FileField(null=True)
    origin = models.CharField(max_length=30, null=True)
