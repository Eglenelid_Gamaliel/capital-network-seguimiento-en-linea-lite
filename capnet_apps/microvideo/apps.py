from django.apps import AppConfig


class MicrovideoConfig(AppConfig):
    name = 'microvideo'
