import os
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
import django_chatter.routing
#import chat.routing

'''
application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': AuthMiddlewareStack(
        URLRouter(
            chat.routing.websocket_urlpatterns
        )
    ),
})
'''
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Capnet_Apps_Slite.settings')

application = ProtocolTypeRouter({
    'websocket': AuthMiddlewareStack(
        URLRouter(
            django_chatter.routing.websocket_urlpatterns
        )
    )
})
