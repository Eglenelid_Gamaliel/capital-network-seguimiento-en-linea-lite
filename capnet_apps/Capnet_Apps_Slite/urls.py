from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', include('seguimientolite.urls')),
    path('file_upload/', include('microvideo.urls')),
    path('tracking/', include('tracker.urls')),
    path('webpush/', include('webpush.urls')),
    path('chat/', include('django_chatter.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
