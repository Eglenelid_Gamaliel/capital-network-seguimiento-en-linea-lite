# Registro de Cambios
Todos los cambios notables del proyecto estan documentados en este documento.

## [Sin publicar]
### [En proceso]
### Añadido.

* [x] Pantalla de configuracion del proyecto.
* [ ] Pantalla de asesor funcional

## {0.5.0} -2019-11-15
### Añadido.
- Accesos al chat desde la pantalla de Asesor.
- Accesos a la pantalla de asesor desde el chat.
- Notificaciones compatibles con Firefox 46+ y Chrome 52+.
- Compatibilidad inicial con IIS.
- Ahora es posible para el cliente acceder al chat desde un link personalizado.

### Cambios.
- Corregido un problema de visualizacion de la pantalla de inicio de sesion en moviles.
- El cliente solo puede ver el chat con su asesor.

### Removido.
- El cliente ya no puede buscar contactos con los que iniciar una conversación.

## [0.4.3] - 2019-11-14
### Añadido.

- Configuracion inicial de notificaciones push.
- Deteccion de la sesion del cliente en el chat.
- Soporte de Emojis para el chat.

### Cambios.
- Corregido un problema de visualizacion en las burbujas del chat.
- Corregido un problema en la visualizacion del tipo de letra en el chat.
- Corregida la presentacion del chat en pantallas grandes.

## [0.4.2] - 2019-11-13
### Añadido.
- Soporte para subir videos al servidor.
- La galeria permite pre-visualizar los videos subidos.
- [Configurable] Los archivos se pueden subir manualmente o automaticamnte.

### Cambios.
- Ya es posible incluir multiples dialogos para subir archivos.

## [0.4.1] - 2019-11-12
### Añadido.
- Galeria para visualizar las fotografias subidas al servidor.

### Cambios.
- Soporte para llenado dinamico de opciones para iOS.
- Correcciones de presentacion en moviles y tablets.
- Corregida la barra de navegacion de la pantalla de tecnico en tablets y telefonos.

## [0.4.0] - 2019-11-11
### Añadido.
- Interfaz basada en Javascript para subir archivos al servidor.
- Proteccion contra 'Cross-site request forgery' ( falsificación de petición en sitios cruzados).

### Cambios.
- La pantalla de tecnico ya no muestra accesos directos a otras pantallas.


## [0.3.0] - 2019-11-07
### Añadido.
- Se creó un repositorio de desarrollo en Gitlab para el seguimiento de cambios, errores y mejoras.
- Se añadió un inicio de sesión para la interfaz de Asesor y Chat.
