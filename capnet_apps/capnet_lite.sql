USE [capnet_bmw_queretaro_lite]
GO
/****** Object:  Table [dbo].[actividades_tecnico_recepcion_activa]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[actividades_tecnico_recepcion_activa](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_hd] [bigint] NULL,
	[placas] [nvarchar](25) NULL,
	[no_orden] [nvarchar](25) NULL,
	[item] [nvarchar](100) NULL,
	[tecnico] [nvarchar](50) NULL,
	[estado] [nvarchar](25) NULL,
	[comentarios] [nvarchar](500) NULL,
	[fecha_hora_inicio] [datetime2](7) NULL,
	[fecha_hora_fin] [datetime2](7) NULL,
	[fecha_hora_actualizacion] [datetime2](7) NULL
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_operaciones_recepcion_activa]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE view [dbo].[v_operaciones_recepcion_activa] as 

select
id_hd,
numcita,
noorden,
horallegada,
noPlacas,
asesor = case when a.idasesor = '' then 'Sin asesor registrado' else (select top 1 nombre from [tablerov3_ford_cumbres].dbo.SccUsuarios where cveAsesor=a.idAsesor) end,
Cliente,
tipocliente,
Vehiculo,
vin,
colorPrisma


from tablerov3_ford_cumbres.dbo.tb_citas_header_nw as a
where Horallegada is not null

--where fecha = cast(getdate() as date)
GO
/****** Object:  View [dbo].[v_operaciones_asesor_recepcion_activa]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE view [dbo].[v_operaciones_asesor_recepcion_activa] as 

select distinct top 100 percent 

fecha_ingreso=b.horallegada,
id_hd= b.id_hd,
cliente=b.cliente,
no_orden = a.no_orden,
placas=b.noplacas,
vin=b.vin,
vehiculo= b.vehiculo,
asesor= b.asesor,
tecnico = a.tecnico

from  [dbo].[actividades_tecnico_recepcion_activa] as a 
inner join [v_operaciones_recepcion_activa] as b on a.id_hd =b.id_hd 
where a.fecha_hora_fin is not null
GO
/****** Object:  Table [dbo].[actividades_tecnico]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[actividades_tecnico](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_hd] [bigint] NULL,
	[no_orden] [nvarchar](25) NULL,
	[item] [nvarchar](100) NULL,
	[tecnico] [nvarchar](50) NULL,
	[estado] [nvarchar](25) NULL,
	[comentarios] [nvarchar](500) NULL,
	[fecha_hora_inicio] [datetime2](7) NULL,
	[fecha_hora_fin] [datetime2](7) NULL,
	[fecha_hora_actualizacion] [datetime2](7) NULL,
	[num_tor] [nvarchar](20) NULL,
 CONSTRAINT [PK__activida__3213E83F3CA610C4] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_operaciones_tecnico]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE view [dbo].[v_operaciones_tecnico] as 

select top 100 percent

	id = row_number() over (order by a.fecha desc),
	id_hd= a.id_hd,
	vin=a.vin,
	no_orden = a.noorden,
	placas= b.noplacas,
	tecnico = t.NOMBRE_EMPLEADO,
	asesor = case when a.idasesor = '' then 'Sin asesor registrado' else (select top 1 nombre from [tablerov3_ford_cumbres].dbo.SccUsuarios where cveAsesor=a.idAsesor) end,
	vehiculo = b.vehiculo,
	fecha_llegada = cast(b.horallegada as date),
	hora_promesa = b.fecha_hora_com,
	cliente=b.cliente,

	servicio_capturado = (case 
	when a.serviciocapturado like '%Servicio%' then 'Servicio'
	when a.servicioCapturado like '%Diagnostico%' then 'Diagnostico'
	when a.servicioCapturado like '%Reparacion%' then 'Reparacion'
	
	else a.servicioCapturado end)	
	
from [tablerov3_ford_cumbres].dbo.tb_citas as a 
join [tablerov3_ford_cumbres].dbo.Tb_CITAS_HEADER_NW as b on a.id_hd = b.id_hd

left join [tablerov3_ford_cumbres].dbo.tb_tecnicos as t on a.idTecnico = t.id_empleado
--left join [tablerov3_ford_cumbres].dbo.tb_citas  as c on c.id_hd=a.id_hd and c.servicioCapturado = 'lavado'

where t.NOMBRE_EMPLEADO<>'NO SHOW'
--and b.HoraRetiro is null
and b.Horallegada is not null
and b.noOrden <> '0'
and b.fecha > '20191101'
and a.servicioCapturado <> 'Lavado'

GO
/****** Object:  View [dbo].[v_status]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	create view [dbo].[v_status] as 
	

	/*calcula los estatus de las ordenes de acuerdo a las fechas-horas de grabado en distintas tablas*/
	
	SELECT  distinct top 100 percent
	
	id_hd = a.id_hd,
	no_orden = a.no_orden,
	placas = a.placas,
	vin = a.vin,

	estatus=
	(case
	when b.fecha_hora_inicio is not null and b.fecha_hora_fin is null then 'En Revisión'
	when b.fecha_hora_inicio is not null and b.fecha_hora_fin is not null then 'En Cotización'
	else 'Pendiente de envio' end)
	
	FROM [dbo].[v_operaciones_tecnico] as a 
	inner join [dbo].[actividades_tecnico] as b on a.no_orden COLLATE DATABASE_DEFAULT =b.no_orden COLLATE DATABASE_DEFAULT

	
GO
/****** Object:  View [dbo].[v_operaciones_refacciones]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE view [dbo].[v_operaciones_refacciones] as 

select distinct 

fecha_ingreso=b.fecha_llegada,
no_orden = a.no_orden,
placas=b.placas,
vin=b.vin,
vehiculo= b.vehiculo,
asesor=b.asesor

from  [dbo].[actividades_tecnico] as a 
left join [v_operaciones_tecnico] as b on a.no_orden collate DATABASE_DEFAULT =b.no_orden collate DATABASE_DEFAULT
where a.fecha_hora_fin is not null

GO
/****** Object:  Table [dbo].[actividades_refacciones_cotizacion_pdf]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[actividades_refacciones_cotizacion_pdf](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[no_orden] [nvarchar](50) NULL,
	[fecha_hora_inicio] [datetime] NULL,
	[fecha_hora_fin] [datetime] NULL,
	[nombre_pdf] [nvarchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_operaciones_asesor]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE view [dbo].[v_operaciones_asesor] as 

select distinct 
id = row_number() over (order by b.fecha_llegada desc),

fecha_ingreso=b.fecha_llegada,
no_orden = b.no_orden,
placas=b.placas,
vin=b.vin,
vehiculo= b.vehiculo,
asesor=b.asesor,
--tecnico = b.tecnico,
fecha_hora_fin_refacciones= c.fecha_hora_fin ---fin de refacciones
--a.num_tor


from [v_operaciones_tecnico] as b
inner join (select distinct no_orden, fecha_hora_fin from [dbo].[actividades_refacciones_cotizacion_pdf]) as c on c.no_orden collate DATABASE_DEFAULT =b.no_orden collate DATABASE_DEFAULT
where c.fecha_hora_fin is not null

--and b.no_orden like '%73697%'

GO
/****** Object:  Table [dbo].[actividades_tecnico_medios]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[actividades_tecnico_medios](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[no_orden] [nvarchar](25) NULL,
	[item] [nvarchar](100) NULL,
	[evidencia] [nvarchar](100) NOT NULL,
	[fecha_hora_subida] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_seguimiento_evidencia]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view  [dbo].[v_seguimiento_evidencia]
as

select distinct top 100 percent
a.*,
b.evidencia

from [dbo].[actividades_tecnico] as a 
left join [dbo].[actividades_tecnico_medios] as b on a.no_orden=b.no_orden and a.item=b.item
GO
/****** Object:  Table [dbo].[actividades_tecnico_medios_recepcion_activa]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[actividades_tecnico_medios_recepcion_activa](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_hd] [bigint] NULL,
	[placas] [nvarchar](25) NULL,
	[no_orden] [nvarchar](25) NULL,
	[item] [nvarchar](100) NULL,
	[evidencia] [nvarchar](100) NOT NULL,
	[fecha_hora_subida] [datetime2](7) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[v_seguimiento_evidencia_recepcion_activa]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view  [dbo].[v_seguimiento_evidencia_recepcion_activa]
as

select distinct top 100 percent
a.*,
b.evidencia

from [dbo].[actividades_tecnico_recepcion_activa] as a 
left join [dbo].[actividades_tecnico_medios_recepcion_activa] as b on a.no_orden=b.no_orden and a.item=b.item
GO
/****** Object:  View [dbo].[ordenes]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[ordenes] as
                select id_sucursal = 1, [id_hd], [NUMCITA] collate DATABASE_DEFAULT as [NUMCITA], [NOORDEN] collate DATABASE_DEFAULT as [NOORDEN], 
                [fecha], [horaAsesor], case when [idasesor] = '' then '- - -' else [idasesor] end collate DATABASE_DEFAULT as [idasesor], [noPlacas] collate DATABASE_DEFAULT as [noPlacas], 
                [vin] collate DATABASE_DEFAULT as [vin], [colorPrisma] collate DATABASE_DEFAULT as [colorPrisma], 
                [Vehiculo] collate DATABASE_DEFAULT as [Vehiculo], [Color] collate DATABASE_DEFAULT as [Color], [Ano], [Cilindros], [Kilometraje], 
                [idCliente] collate DATABASE_DEFAULT as [idCliente], [Cliente] collate DATABASE_DEFAULT as [Cliente], 
                [tipoCliente] collate DATABASE_DEFAULT as [tipoCliente], [telefonos] collate DATABASE_DEFAULT as [telefonos], 
                [ContactoNombre] collate DATABASE_DEFAULT as [ContactoNombre], [ContactoTelefono] collate DATABASE_DEFAULT as [ContactoTelefono], 
                [horaTolerancia], [Horallegada], [HoraIRecepcion], [HoraFRecepcion], [HoraIEntrega], [HoraFEntrega], [HoraRetiro], [horaRampa], 
                [fechaHoraPromesa], [Status] collate DATABASE_DEFAULT as [Status], [Fecha_hora_Status], [idOp], [bahia], 
                [OBSERVACIONES] collate DATABASE_DEFAULT as [OBSERVACIONES], [USUARIO] collate DATABASE_DEFAULT as [USUARIO], [FECHA_AGENDADO], 
                [FECHA_ORIGINAL], [fecha_hora_apertura_os], [fecha_hora_cierre_os], [Fecha_hora_com], 
                [Status_OS] collate DATABASE_DEFAULT as [Status_OS], [seriecolorprisma], [testQA], [tipolavado] collate DATABASE_DEFAULT as [tipolavado], 
                [comentariosLavado] collate DATABASE_DEFAULT as [comentariosLavado] 
                from tablerov3_ford_cumbres.dbo.tb_citas_header_nw
GO
/****** Object:  View [dbo].[tecnicos]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[tecnicos] as
      select *
      from tablerov3_ford_cumbres.dbo.tb_tecnicos
GO

/****** Object:  View [dbo].[v_tecnicos]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create view [dbo].[v_tecnicos] as 


select * from tablerov3_ford_cumbres.dbo.tb_tecnicos

GO
/****** Object:  View [dbo].[v_tracker]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*------------------------------------------------------------------*/
/*------------------Autor: Eliu Gtz/Gamaliel Gtz--------------------*/
/*la vista alimenta el sistema de Order Tracking de CN--------------*/

CREATE view [dbo].[v_tracker] as 

select

	id = row_number() over (order by a.fecha desc),

	id_hd= a.id_hd,
	no_orden = b.noorden,
	placas= b.noplacas,
	fecha =cast(b.fecha as date),
	telefono_agencia = '52922828',
	tecnico = t.NOMBRE_EMPLEADO,
	asesor = case when a.idasesor = '' then 'Sin asesor registrado'
			when a.idAsesor is null then 'Sin asesor registrado'
	else (select top 1 nombre from [tablerov3_ford_cumbres].dbo.SccUsuarios where cveAsesor=a.idAsesor) end,
	vehiculo = b.vehiculo,
	hora_llegada = b.horallegada,
	hora_inicio_asesor = b.horairecepcion,
	hora_fin_asesor = b.horafrecepcion,
	hora_promesa = b.fecha_hora_com,
	hora_grabado=a.horarampa,
	
	--inicio y fin para técnicos
	inicio_tecnico = a.fecha_hora_ini_oper,
	fin_tecnico = (case when a.fecha_hora_paro is null then a.Fecha_Hora_Fin_Oper else a.fecha_hora_paro end),
	detenido = (case when a.fecha_hora_paro is not null then 'True' else 'False' end ), 

	servicio_capturado = (case 
	when a.serviciocapturado like '%Servicio%' then 'Servicio'
	when a.servicioCapturado like '%Diagnostico%' then 'Diagnostico'
	when a.servicioCapturado like '%Reparacion%' then 'Reparacion'
	
	else a.servicioCapturado end),

	--inicio y fin de lavado
	inicio_tecnico_lavado = c.fecha_hora_ini_oper,
	fin_tecnico_lavado = c.fecha_hora_fin_oper,

	ultima_actualizacion = (case
	
	when b.horairecepcion is null then DATEDIFF(minute , b.horallegada, getdate()) 
	when b.horaFrecepcion is null then DATEDIFF(minute,  b.horairecepcion, getdate())
	when a.fecha_hora_ini_oper is null then DATEDIFF(minute, b.horaFrecepcion, getdate())
	when a.fecha_hora_fin_oper is null then DATEDIFF(minute, a.fecha_hora_ini_oper, getdate())
	when c.fecha_hora_ini_oper is null then DATEDIFF(minute, a.fecha_hora_fin_oper, getdate())
	when c.fecha_hora_fin_oper is null then DATEDIFF(minute, c.fecha_hora_ini_oper, getdate())
	else  DATEDIFF(minute, c.fecha_hora_fin_oper, getdate())
	
	end),

	motivo_paro=(case 

	when a.id_motivo_paro='2' then 'Refacciones' 
	when a.id_motivo_paro='3' then 'Autorización'
	when a.id_motivo_paro='4' then 'Proceso'
	when a.id_motivo_paro='7' then 'Trabajo en otro taller'
	
	end)
	
	
from [tablerov3_ford_cumbres].dbo.tb_citas as a 
join [tablerov3_ford_cumbres].dbo.Tb_CITAS_HEADER_NW as b on a.id_hd = b.id_hd

left join [tablerov3_ford_cumbres].dbo.tb_tecnicos as t on a.idTecnico = t.id_empleado
left join [tablerov3_ford_cumbres].dbo.tb_citas  as c on c.id_hd=a.id_hd and c.servicioCapturado = 'lavado'

where t.NOMBRE_EMPLEADO<>'NO SHOW'
and b.HoraRetiro is null
and b.Horallegada is not null
and b.noOrden <> 0
and b.fecha > '20190101'
and a.servicioCapturado <> 'Lavado'
and b.HoraIRecepcion is not null

GO
/****** Object:  View [dbo].[v_usuarios]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[v_usuarios] as

select a.*,
id = row_number() over (order by a.pass desc),
last_login='',
Is_superuser= case when a.cvePerfil=1 then 1 else 0 end,
last_name= getdate(),
is_staff= 1,
is_active= 1,
date_joined= cast('20190101' as datetime)



from tablerov3_ford_cumbres.dbo.sccusuarios as a
GO
/****** Object:  Table [dbo].[actividades_asesor_recepcion_activa]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[actividades_asesor_recepcion_activa](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_hd] [bigint] NULL,
	[no_orden] [nvarchar](25) NULL,
	[placas] [nvarchar](25) NULL,
	[vin] [nvarchar](25) NULL,
	[cliente] [nvarchar](50) NULL,
	[vehiculo] [nvarchar](50) NULL,
	[asesor] [nvarchar](50) NULL,
	[tecnico] [nvarchar](50) NULL,
	[item] [nvarchar](50) NULL,
	[estado] [nvarchar](50) NULL,
	[comentarios] [nvarchar](200) NULL,
	[mo] [decimal](18, 0) NULL,
	[refacciones] [decimal](18, 0) NULL,
	[total] [decimal](18, 0) NULL,
	[fecha_hora_guardado] [datetime] NULL,
	[fecha_hora_modificacion] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[actividades_cliente]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[actividades_cliente](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_hd] [bigint] NULL,
	[no_orden] [nvarchar](25) NULL,
	[placas] [nvarchar](25) NULL,
	[item] [nvarchar](50) NULL,
	[estado] [nvarchar](50) NULL,
	[autorizacion] [nvarchar](50) NULL,
	[fecha_hora_autorizacion] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_group]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](150) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [auth_group_name_a6ea08ec_uniq] UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_group_permissions]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_group_permissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NOT NULL,
	[permission_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [auth_group_permissions_group_id_permission_id_0cd325b0_uniq] UNIQUE NONCLUSTERED 
(
	[group_id] ASC,
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_permission]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_permission](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[content_type_id] [int] NOT NULL,
	[codename] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [auth_permission_content_type_id_codename_01ab375a_uniq] UNIQUE NONCLUSTERED 
(
	[content_type_id] ASC,
	[codename] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_user]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[password] [nvarchar](128) NOT NULL,
	[last_login] [datetime2](7) NULL,
	[is_superuser] [bit] NOT NULL,
	[username] [nvarchar](150) NOT NULL,
	[first_name] [nvarchar](30) NOT NULL,
	[last_name] [nvarchar](150) NOT NULL,
	[email] [nvarchar](254) NOT NULL,
	[is_staff] [bit] NOT NULL,
	[is_active] [bit] NOT NULL,
	[date_joined] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [auth_user_username_6821ab7c_uniq] UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_user_groups]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_user_groups](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[group_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [auth_user_groups_user_id_group_id_94350c0c_uniq] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[group_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auth_user_user_permissions]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auth_user_user_permissions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[permission_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [auth_user_user_permissions_user_id_permission_id_14a6b632_uniq] UNIQUE NONCLUSTERED 
(
	[user_id] ASC,
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_admin_log]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_admin_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[action_time] [datetime2](7) NOT NULL,
	[object_id] [nvarchar](max) NULL,
	[object_repr] [nvarchar](200) NOT NULL,
	[action_flag] [smallint] NOT NULL,
	[change_message] [nvarchar](max) NOT NULL,
	[content_type_id] [int] NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_chatter_message]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_chatter_message](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_created] [datetime2](7) NOT NULL,
	[date_modified] [datetime2](7) NOT NULL,
	[text] [nvarchar](max) NOT NULL,
	[room_id] [char](32) NOT NULL,
	[sender_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_chatter_message_recipients]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_chatter_message_recipients](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[message_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [django_chatter_message_recipients_message_id_user_id_50922e74_uniq] UNIQUE NONCLUSTERED 
(
	[message_id] ASC,
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_chatter_room]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_chatter_room](
	[date_created] [datetime2](7) NOT NULL,
	[date_modified] [datetime2](7) NOT NULL,
	[id] [char](32) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_chatter_room_members]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_chatter_room_members](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[room_id] [char](32) NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [django_chatter_room_members_room_id_user_id_51259eac_uniq] UNIQUE NONCLUSTERED 
(
	[room_id] ASC,
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_chatter_userprofile]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_chatter_userprofile](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[last_visit] [datetime2](7) NOT NULL,
	[user_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_content_type]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_content_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[app_label] [nvarchar](100) NOT NULL,
	[model] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [django_content_type_app_label_model_76bd3d3b_uniq] UNIQUE NONCLUSTERED 
(
	[app_label] ASC,
	[model] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_migrations]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_migrations](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[app] [nvarchar](255) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[applied] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[django_session]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[django_session](
	[session_key] [nvarchar](40) NOT NULL,
	[session_data] [nvarchar](max) NOT NULL,
	[expire_date] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[session_key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[log_cliente]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[log_cliente](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_hd] [bigint] NULL,
	[no_orden] [varchar](25) NULL,
	[cliente] [varbinary](50) NULL,
	[fecha_hora_visita] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[log_envios]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[log_envios](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[no_orden] [nvarchar](25) NULL,
	[medio] [nvarchar](50) NULL,
	[fecha_hora_envio] [datetime] NULL,
	[telefono] [nvarchar](50) NULL,
	[correo] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[seguimientolite_actividades_tecnico]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[seguimientolite_actividades_tecnico](
	[id] [bigint] NOT NULL,
	[no_orden] [nvarchar](25) NULL,
	[item] [nvarchar](100) NULL,
	[estado] [nvarchar](25) NULL,
	[comentarios] [nvarchar](500) NULL,
	[fecha_hora_inicio] [datetime2](7) NOT NULL,
	[fecha_hora_fin] [datetime2](7) NOT NULL,
	[fecha_hora_actualizacion] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[seguimientolite_actividades_tecnicomedios]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[seguimientolite_actividades_tecnicomedios](
	[id] [bigint] NOT NULL,
	[no_orden] [nvarchar](25) NULL,
	[item] [nvarchar](100) NULL,
	[evidencia] [nvarchar](100) NOT NULL,
	[fecha_hora_subida] [datetime2](7) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[seguimientolite_audio]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[seguimientolite_audio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sound] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[seguimientolite_photo]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[seguimientolite_photo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[image] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpush_group]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpush_group](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpush_pushinformation]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpush_pushinformation](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[group_id] [int] NULL,
	[subscription_id] [int] NOT NULL,
	[user_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[webpush_subscriptioninfo]    Script Date: 15/12/2019 23:56:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[webpush_subscriptioninfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[browser] [nvarchar](100) NOT NULL,
	[endpoint] [nvarchar](500) NOT NULL,
	[auth] [nvarchar](100) NOT NULL,
	[p256dh] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[actividades_tecnico]  WITH CHECK ADD  CONSTRAINT [FK_actividades_tecnico_actividades_tecnico] FOREIGN KEY([id])
REFERENCES [dbo].[actividades_tecnico] ([id])
GO
ALTER TABLE [dbo].[actividades_tecnico] CHECK CONSTRAINT [FK_actividades_tecnico_actividades_tecnico]
GO
ALTER TABLE [dbo].[auth_group_permissions]  WITH CHECK ADD  CONSTRAINT [auth_group_permissions_group_id_b120cbf9_fk_auth_group_id] FOREIGN KEY([group_id])
REFERENCES [dbo].[auth_group] ([id])
GO
ALTER TABLE [dbo].[auth_group_permissions] CHECK CONSTRAINT [auth_group_permissions_group_id_b120cbf9_fk_auth_group_id]
GO
ALTER TABLE [dbo].[auth_group_permissions]  WITH CHECK ADD  CONSTRAINT [auth_group_permissions_permission_id_84c5c92e_fk_auth_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[auth_permission] ([id])
GO
ALTER TABLE [dbo].[auth_group_permissions] CHECK CONSTRAINT [auth_group_permissions_permission_id_84c5c92e_fk_auth_permission_id]
GO
ALTER TABLE [dbo].[auth_permission]  WITH CHECK ADD  CONSTRAINT [auth_permission_content_type_id_2f476e4b_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[auth_permission] CHECK CONSTRAINT [auth_permission_content_type_id_2f476e4b_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[auth_user_groups]  WITH CHECK ADD  CONSTRAINT [auth_user_groups_group_id_97559544_fk_auth_group_id] FOREIGN KEY([group_id])
REFERENCES [dbo].[auth_group] ([id])
GO
ALTER TABLE [dbo].[auth_user_groups] CHECK CONSTRAINT [auth_user_groups_group_id_97559544_fk_auth_group_id]
GO
ALTER TABLE [dbo].[auth_user_groups]  WITH CHECK ADD  CONSTRAINT [auth_user_groups_user_id_6a12ed8b_fk_auth_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[auth_user_groups] CHECK CONSTRAINT [auth_user_groups_user_id_6a12ed8b_fk_auth_user_id]
GO
ALTER TABLE [dbo].[auth_user_user_permissions]  WITH CHECK ADD  CONSTRAINT [auth_user_user_permissions_permission_id_1fbb5f2c_fk_auth_permission_id] FOREIGN KEY([permission_id])
REFERENCES [dbo].[auth_permission] ([id])
GO
ALTER TABLE [dbo].[auth_user_user_permissions] CHECK CONSTRAINT [auth_user_user_permissions_permission_id_1fbb5f2c_fk_auth_permission_id]
GO
ALTER TABLE [dbo].[auth_user_user_permissions]  WITH CHECK ADD  CONSTRAINT [auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[auth_user_user_permissions] CHECK CONSTRAINT [auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_content_type_id_c4bce8eb_fk_django_content_type_id] FOREIGN KEY([content_type_id])
REFERENCES [dbo].[django_content_type] ([id])
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_content_type_id_c4bce8eb_fk_django_content_type_id]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_user_id_c564eba6_fk_auth_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_user_id_c564eba6_fk_auth_user_id]
GO
ALTER TABLE [dbo].[django_chatter_message]  WITH CHECK ADD  CONSTRAINT [django_chatter_message_room_id_d7ab10ce_fk_django_chatter_room_id] FOREIGN KEY([room_id])
REFERENCES [dbo].[django_chatter_room] ([id])
GO
ALTER TABLE [dbo].[django_chatter_message] CHECK CONSTRAINT [django_chatter_message_room_id_d7ab10ce_fk_django_chatter_room_id]
GO
ALTER TABLE [dbo].[django_chatter_message]  WITH CHECK ADD  CONSTRAINT [django_chatter_message_sender_id_7ed6d63f_fk_auth_user_id] FOREIGN KEY([sender_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[django_chatter_message] CHECK CONSTRAINT [django_chatter_message_sender_id_7ed6d63f_fk_auth_user_id]
GO
ALTER TABLE [dbo].[django_chatter_message_recipients]  WITH CHECK ADD  CONSTRAINT [django_chatter_message_recipients_message_id_ded17917_fk_django_chatter_message_id] FOREIGN KEY([message_id])
REFERENCES [dbo].[django_chatter_message] ([id])
GO
ALTER TABLE [dbo].[django_chatter_message_recipients] CHECK CONSTRAINT [django_chatter_message_recipients_message_id_ded17917_fk_django_chatter_message_id]
GO
ALTER TABLE [dbo].[django_chatter_message_recipients]  WITH CHECK ADD  CONSTRAINT [django_chatter_message_recipients_user_id_75e70a0a_fk_auth_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[django_chatter_message_recipients] CHECK CONSTRAINT [django_chatter_message_recipients_user_id_75e70a0a_fk_auth_user_id]
GO
ALTER TABLE [dbo].[django_chatter_room_members]  WITH CHECK ADD  CONSTRAINT [django_chatter_room_members_room_id_2a48478d_fk_django_chatter_room_id] FOREIGN KEY([room_id])
REFERENCES [dbo].[django_chatter_room] ([id])
GO
ALTER TABLE [dbo].[django_chatter_room_members] CHECK CONSTRAINT [django_chatter_room_members_room_id_2a48478d_fk_django_chatter_room_id]
GO
ALTER TABLE [dbo].[django_chatter_room_members]  WITH CHECK ADD  CONSTRAINT [django_chatter_room_members_user_id_3decc4c1_fk_auth_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[django_chatter_room_members] CHECK CONSTRAINT [django_chatter_room_members_user_id_3decc4c1_fk_auth_user_id]
GO
ALTER TABLE [dbo].[django_chatter_userprofile]  WITH CHECK ADD  CONSTRAINT [django_chatter_userprofile_user_id_e8ce360a_fk_auth_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[django_chatter_userprofile] CHECK CONSTRAINT [django_chatter_userprofile_user_id_e8ce360a_fk_auth_user_id]
GO
ALTER TABLE [dbo].[webpush_pushinformation]  WITH CHECK ADD  CONSTRAINT [webpush_pushinformation_group_id_262dcc9a_fk_webpush_group_id] FOREIGN KEY([group_id])
REFERENCES [dbo].[webpush_group] ([id])
GO
ALTER TABLE [dbo].[webpush_pushinformation] CHECK CONSTRAINT [webpush_pushinformation_group_id_262dcc9a_fk_webpush_group_id]
GO
ALTER TABLE [dbo].[webpush_pushinformation]  WITH CHECK ADD  CONSTRAINT [webpush_pushinformation_subscription_id_7989aa34_fk_webpush_subscriptioninfo_id] FOREIGN KEY([subscription_id])
REFERENCES [dbo].[webpush_subscriptioninfo] ([id])
GO
ALTER TABLE [dbo].[webpush_pushinformation] CHECK CONSTRAINT [webpush_pushinformation_subscription_id_7989aa34_fk_webpush_subscriptioninfo_id]
GO
ALTER TABLE [dbo].[webpush_pushinformation]  WITH CHECK ADD  CONSTRAINT [webpush_pushinformation_user_id_5e083b7f_fk_auth_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[auth_user] ([id])
GO
ALTER TABLE [dbo].[webpush_pushinformation] CHECK CONSTRAINT [webpush_pushinformation_user_id_5e083b7f_fk_auth_user_id]
GO
ALTER TABLE [dbo].[django_admin_log]  WITH CHECK ADD  CONSTRAINT [django_admin_log_action_flag_a8637d59_check] CHECK  (([action_flag]>=(0)))
GO
ALTER TABLE [dbo].[django_admin_log] CHECK CONSTRAINT [django_admin_log_action_flag_a8637d59_check]
GO
