# Capital Nerwork | Seguimiento En Linea Lite

Este proyecto gestiona vistas para los asesores, tecnicos y clientes en un proyecto desarrollado en Python con el framework web Django.

### Pre-Requisitos

- Python 3.6 o superior
- Redis

Python

```
https://www.python.org/downloads/
```

Redis
- Debian

```
sudo apt inatall redis
```
- Arch
```
sudo pacman -S redis
```
- Windows
```
https://github.com/microsoftarchive/redis/releases
```

### Instalación

Abre una ventana de linea de comandos en la carpeta del proyecto y crea un entorno virtual de Python.

```
python -m venv venv
```

Activa el entorno virtual.
- Linux 
```
source venv/bin/activate
```
- Windows
```
.\venv\Scripts\Activate
```

Instala las dependencias.
```
python -m pip install -r requirements.txt
```

Inicia el servidor de Redis.
```
redis-server
```

Inicia el servidor de desarrollo
```
python manage.py runserver
```

## Despliegue

En construccion.
