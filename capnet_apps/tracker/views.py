from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render
from .models import VTracker
from .forms import OrderForm
from django.core.mail import send_mail
from django.conf import settings
from datetime import date


def detalles_orden(request, no_orden):
    query = VTracker.objects.filter(pk=no_orden)
    chips = int(query.count())
    operaciones = None
    if chips == 1:
        details = query.get()
    elif chips > 1:
        details = query.order_by('-inicio_tecnico')[:1].get()
        operaciones = query.order_by('inicio_tecnico')[0:(chips-1)]    
    elif not chips:
        raise Http404("No Se Encontro La Orden Solicitada")

    e_recepcion = "completed"
    e_asesor = "inactive"
    e_tecnico = "inactive"
    e_lavado = "inactive"
    e_entrega = "inactive"

    print(chips)

    estados = [
        details.hora_inicio_asesor,
        details.hora_fin_asesor,
        details.inicio_tecnico,
        details.fin_tecnico,
        details.inicio_tecnico_lavado,
        details.fin_tecnico_lavado
    ]
    mensajes = [
        'Tu vehículo se encuentra en manos de un asesor.',
        'En breve tu vehículo ingresara al taller de servicio.',
        'Tu vehículo se encuentra en manos de un experto técnico',
        'Tu vehículo ha salido del taller de servicio,  ingresara al area de lavado en breve.',
        'En unos momentos mas tu vehículo estará listo para ser entregado.',
        'Tu vehículo esta listo',
        'Tu vehículo se encuentra detenido',
        'Su vehículo se encuentra detenido debido a que se necesita su autorizacion para continuar el proceso.',
        'En breve su asesor se comunicara con usted',
        '(Mensaje de pruebaruta)',
        '(Mensaje de cal+idad)',
        '(Mensaje de tot)',
    ]

    inactivos = [i for i,x in enumerate(estados) if not x]

    if details.ultima_actualizacion<60:
        min = details.ultima_actualizacion
        if min == 1:
            u_actualizacion = f'{min:.0f} Minuto'
        else:
            u_actualizacion = f'{min:.0f} Minutos'
    elif details.ultima_actualizacion>60 and details.ultima_actualizacion<1440:
        min = details.ultima_actualizacion/60
        if min < 2:
            u_actualizacion = f'{min:.0f} Hora'
        else:
            u_actualizacion = f'{min:.0f} Horas'
    elif details.ultima_actualizacion>1440:
        min = details.ultima_actualizacion/1440
        if min < 2:
            u_actualizacion = f'{min:.0f} Dia'
        else:
            u_actualizacion = f'{min:.0f} Dias'

    if inactivos == []:
        e_recepcion = "completed"
        e_asesor = "completed"
        e_tecnico = "completed"
        e_lavado = "completed"
        if details.motivo_paro is None:
            e_entrega = "active"    
            serv_actual = "Status Actual: Listo Para Entrega"
            m_actual = mensajes[5]
        elif details.motivo_paro == 'Autorización':
            e_entrega = "inactive"
            serv_actual = "Status actual: Detenido Por Autorización"
            m_actual = mensajes[7]
        else:
            e_entrega = "inactive"
            serv_actual = "Status actual: Detenido"
            m_actual = mensajes[6]

    elif inactivos[0] == 1:
        if details.motivo_paro is None:
            e_asesor = "active"
            serv_actual = "Status Actual: Asesor"
            m_actual = mensajes[0]
        elif details.motivo_paro == 'Autorización':
            e_asesor = "inactive"
            serv_actual = "Status Actual: Detenido Por Autorización"
            m_actual = mensajes[7]
        else:
            e_asesor = "inactive"
            serv_actual = "Status Actual: Detenido"
            m_actual = mensajes[6]


    elif inactivos[0] == 2:
        if details.motivo_paro is None:
            e_asesor = "completed"
            serv_actual = "Ultimo Status: Asesor"
            m_actual = mensajes[1]
        elif details.motivo_paro == 'Autorización':
            e_asesor = "inactive"
            serv_actual = "Status Actual: Detenido Por Autorización"
            m_actual = mensajes[7]
        else:
            e_asesor = "inactive"
            serv_actual = "Ultimo Status: Detenido"
            m_actual = mensajes[6]

    elif inactivos[0] == 3:
        e_asesor = "completed"
        if details.motivo_paro is None:
            e_tecnico = "active"
            serv_actual = "Status Actual: Servicio"
            m_actual = mensajes[2]
        elif details.motivo_paro == 'Autorización':
            e_tecnico = "inactive"
            serv_actual = "Status Actual: Detenido Por Autorización"
            m_actual = mensajes[7]
        else:
            e_tecnico = "inactive"
            serv_actual = "Status Actual: Detenido"
            m_actual = mensajes[6]

    elif inactivos[0] == 4:
        e_asesor = "completed"
        if details.motivo_paro is None:
            e_tecnico = "completed"
            serv_actual = "Ultimo Status: Servicio"
            m_actual = mensajes[3]
        elif details.motivo_paro == 'Autorización':
            e_tecnico = "inactive"
            serv_actual = "Status Actual: Detenido Por Autorización"
            m_actual = mensajes[7]
        else:
            e_tecnico = "inactive"
            serv_actual = "Ultimo Status: Detenido"
            m_actual = mensajes[6]

    elif inactivos[0] == 5:
        e_asesor = "completed"
        e_tecnico = "completed"
        if details.motivo_paro is None:
            e_lavado = "active"
            serv_actual = "Status Actual: Lavado"
            m_actual = mensajes[4]
        elif details.motivo_paro == 'Autorización':
            e_lavado = "inactive"
            serv_actual = "Status Actual: Detenido Por Autorización"
            m_actual = mensajes[7]
        else:
            e_lavado = "inactive"
            serv_actual = "Status Actual: Detenido"
            m_actual = mensajes[6]
    context = {
        'chips':chips,
        'details': details,
        'e_recepcion':e_recepcion,
        'e_asesor': e_asesor,
        'e_tecnico': e_tecnico,
        'e_lavado': e_lavado,
        'e_entrega': e_entrega,
        'serv_actual':serv_actual,
        'm_actual':m_actual,
        'u_actualizacion': u_actualizacion,
        'range': range(1, (chips)),
        'operaciones': operaciones,
    }
    return render(request, 'tracker/tracker.html', context)

def search(request):
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            order = data['order_n']
            return HttpResponseRedirect(f'/tracking/{order}')
    else:
        form = OrderForm()
    return render(request, 'tracker/search.html', {'form': form})

def mail(request):
    #query = VTracker.objects.filter(fecha=date.today()).values('no_orden')
    #query = VTracker.objects.order_by('no_o    rden')[:25]
    #print(query)
    query = VTracker.objects.order_by('-no_orden').distinct()[:12].values('placas','no_orden')
    warning = None
    success = None
    if request.method == 'POST':
        print('Post detectado')
        order = list(request.POST.keys())[-1]
        print(order)
        if request.POST.get(f'{order}'):
            user_mail = request.POST.get(f'{order}')
            success = True
            print(user_mail)
            url = f'http://192.168.1.72:8010/tracking/{order}'
            try:
                send_mail(
                    'BMW Queretaro | Segimiento de Servicio en Línea',
                    f'Puede consultar el progreso de su vehiculo en la liga : {url}',
                    settings.EMAIL_HOST_USER,
                    [user_mail]
                )
                print('mensaje enviado')
            except EnvironmentError:
                pass
        else:
            warning = True
    #query = VTracker.objects.filter(fecha=date.today()).values('no_orden')
    return render(request, 'tracker/mail.html', context={'query': query, 'success':success, 'warning': warning})


def tracker_hyp(request, no_orden):
    context = {}
    if request.method == 'GET':
        try:
            queryset = VTracker.objects.filter(noorden=no_orden).order_by('-fecha_hora_fin_oper')
            #fases = queryset.values_list('fase', flat=True).distinct()
            #for fase in fases:
            context['info'] = queryset.first()
            context['rows'] = queryset
        except Exception as e:
            print(e)
    return render(request, 'tracker/hyp.html', context)
