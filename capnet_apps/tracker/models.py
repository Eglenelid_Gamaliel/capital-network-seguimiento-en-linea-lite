# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models

'''
class VTracker(models.Model):
    no_orden = models.CharField(
        max_length=25, blank=True, null=False, primary_key=True)
    placas = models.CharField(max_length=10, blank=True, null=True)
    fecha = models.DateField(blank=True, null=True)
    tecnico = models.CharField(max_length=60, blank=True, null=True)
    asesor = models.CharField(max_length=150, blank=True, null=True)
    vehiculo = models.CharField(max_length=250, blank=True, null=True)
    hora_llegada = models.DateTimeField(blank=True, null=True)
    hora_inicio_asesor = models.DateTimeField(blank=True, null=True)
    hora_fin_asesor = models.DateTimeField(blank=True, null=True)
    hora_promesa = models.DateTimeField(blank=True, null=True)
    hora_grabado = models.CharField(max_length=5, blank=True, null=True)
    inicio_tecnico = models.DateTimeField(blank=True, null=True)
    fin_tecnico = models.DateTimeField(blank=True, null=True)
    detenido = models.CharField(max_length=5)
    servicio_capturado = models.CharField(
        max_length=500, blank=True, null=True)
    inicio_tecnico_lavado = models.DateTimeField(blank=True, null=True)
    fin_tecnico_lavado = models.DateTimeField(blank=True, null=True)
    ultima_actualizacion = models.IntegerField(blank=True, null=True)
    motivo_paro = models.CharField(max_length=22, blank=True, null=True)
    id_hd = models.CharField(max_length=25, blank=False, null=False)
    telefono_agencia = models.CharField(max_length=25, blank=False, null=False)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_tracker'
'''


class VTracker(models.Model):
    id = models.AutoField(primary_key=True)
    #id_orden = models.DecimalField(db_column='id', max_digits=18, decimal_places=0)
    noplacas = models.CharField(max_length=10, blank=True, null=True)
    vin = models.CharField(max_length=50, blank=True, null=True)
    idasesor = models.CharField(db_column='idAsesor', max_length=10, blank=True, null=True)  # Field name made lowercase.
    asesor = models.CharField(max_length=150, blank=True, null=True)
    noorden = models.CharField(max_length=25, blank=True, null=True)
    servicio = models.CharField(max_length=25, blank=True, null=True)
    id_hd = models.IntegerField(blank=True, null=True)
    idservicio = models.CharField(max_length=15, blank=True, null=True)
    fase = models.CharField(max_length=50, blank=True, null=True)
    idtecnico = models.CharField(db_column='idTecnico', max_length=10, blank=True, null=True)  # Field name made lowercase.
    tecnico = models.CharField(max_length=50, blank=True, null=True)
    status = models.CharField(max_length=15, blank=True, null=True)
    fecha_hora_ini_oper = models.DateTimeField(blank=True, null=True)
    fecha_hora_fin_oper = models.DateTimeField(blank=True, null=True)
    fecha_hora_paro = models.DateTimeField(blank=True, null=True)
    id_motivo_paro = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = 'v_tracker'
