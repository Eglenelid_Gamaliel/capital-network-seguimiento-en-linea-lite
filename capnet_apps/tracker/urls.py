from django.urls import path

from . import views

urlpatterns = [
    path('<int:no_orden>', views.detalles_orden, name='Detalles de Orden'),
    path('', views.search, name='search'),
    path('mail', views.mail, name='mail'),
    path('hyp/<str:no_orden>', views.tracker_hyp, name='hyp')
]
